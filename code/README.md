# README #

Projeto de Graduação de Thiago Rocha Salvatore: _AlocaWeb e BibLattes – Módulos do sistema Marvin_.

### Resumo ###

O Departamento de Informática da Universidade Federal do Espírito Santo (DI/Ufes) precisa de sistemas capazes de apoiar certos processos internos (relacionados ao contexto acadêmico), a saber: Alocação de Salas de Aula e Controle de Publicações. O propósito do primeiro é facilitar e otimizar o processo de alocação de salas de aula a turmas, evitando conflitos de horários, por exemplo. Já o segundo tem o propósito de facilitar a extração e importação de informações de um currículo Lattes, possibilitando que o usuário gere um arquivo no formato BibTeX, que pode ser utilizado futuramente em websites, por exemplo.

Para a construção do sistema, seguiu-se um processo de Engenharia de Software realizando as etapas de levantamento de requisitos, especificação de requisitos, definição da arquitetura do sistema, implementação e testes. Foram colocadas em prática as disciplinas aprendidas no decorrer do curso, tais como Engenharia de Software, Engenharia de Requisitos, Projeto de Sistema de Software, Programação Orientada a Objetos e Desenvolvimento Web e Web Semântica. Também foram utilizados métodos e técnicas de modelagem e desenvolvimento orientado a objetos, em particular o método FrameWeb para projeto de aplicações Web baseadas em frameworks.

