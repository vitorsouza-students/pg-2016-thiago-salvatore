package br.ufes.inf.nemo.marvin.biblattes.domain;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-08-24T18:23:47.835-0300")
@StaticMetamodel(Proceeding.class)
public class Proceeding_ extends Publication_ {
	public static volatile SingularAttribute<Proceeding, String> bookTitle;
	public static volatile SingularAttribute<Proceeding, String> doi;
	public static volatile SingularAttribute<Proceeding, String> pages;
	public static volatile SingularAttribute<Proceeding, Academic> owner;
}
