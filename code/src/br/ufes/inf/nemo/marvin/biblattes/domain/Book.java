package br.ufes.inf.nemo.marvin.biblattes.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;

@Entity
public class Book extends Publication {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotNull
	
	private String edition;
		
	@ManyToOne
	private Academic owner;


	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public Academic getOwner() {
		return owner;
	}

	public void setOwner(Academic owner) {
		this.owner = owner;
	}

}
