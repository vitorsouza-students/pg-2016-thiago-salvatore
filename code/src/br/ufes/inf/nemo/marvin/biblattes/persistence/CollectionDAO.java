package br.ufes.inf.nemo.marvin.biblattes.persistence;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.biblattes.domain.Collection;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;

@Local
public interface CollectionDAO extends BaseDAO<Collection> {

	public Collection getCollectionByTitle(String title);

}
