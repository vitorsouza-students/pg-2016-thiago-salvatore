package br.ufes.inf.nemo.marvin.biblattes.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;

@Entity
public class Proceeding extends Publication {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String bookTitle;
	
	private String doi;
	
	private String pages;
	
	@ManyToOne
	private Academic owner;

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getDoi() {
		return doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}

	public Academic getOwner() {
		return owner;
	}

	public void setOwner(Academic owner) {
		this.owner = owner;
	}

	public String getPages() {
		return pages;
	}

	public void setPages(String pages) {
		this.pages = pages;
	}
}
