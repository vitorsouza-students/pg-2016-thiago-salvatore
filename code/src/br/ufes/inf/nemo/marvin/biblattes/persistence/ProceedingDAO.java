package br.ufes.inf.nemo.marvin.biblattes.persistence;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.biblattes.domain.Proceeding;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;

@Local
public interface ProceedingDAO extends BaseDAO<Proceeding> {

	public Proceeding getProceedingByTitle(String title);

	public Proceeding getProceedingByDOI(String doi);

}
