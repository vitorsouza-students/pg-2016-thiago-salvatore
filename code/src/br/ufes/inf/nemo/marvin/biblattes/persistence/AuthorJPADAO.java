package br.ufes.inf.nemo.marvin.biblattes.persistence;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.ufes.inf.nemo.marvin.biblattes.domain.Author;
import br.ufes.inf.nemo.marvin.biblattes.domain.Collection;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;

@Stateless
public class AuthorJPADAO extends BaseJPADAO<Author> implements AuthorDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Class<Author> getDomainClass() {
		return Author.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Override
	public Author retrieveAuthorByName(String name) {
		String jpql = "SELECT a FROM Author a where a.name = :name";
		TypedQuery<Author> query = entityManager.createQuery(jpql, Author.class);
		query.setParameter("name", name);
		
		if(query.getResultList().size()>0){
			return query.getResultList().get(0);
		}
		return null;
	}

}
