package br.ufes.inf.nemo.marvin.biblattes.persistence;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.biblattes.domain.Author;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;

@Local
public interface AuthorDAO extends BaseDAO<Author> {

	public Author retrieveAuthorByName(String name);

}
