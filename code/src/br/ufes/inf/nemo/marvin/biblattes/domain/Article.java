package br.ufes.inf.nemo.marvin.biblattes.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;

@Entity
public class Article extends Publication {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String journal;
	
	private String volume;
	
	private String pages;
	
	private String doi;
	
	private String issn;
	
	@ManyToOne
	private Academic owner;
	
	public String getJournal() {
		return journal;
	}

	public void setJournal(String journal) {
		this.journal = journal;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getPages() {
		return pages;
	}

	public void setPages(String pages) {
		this.pages = pages;
	}

	public String getDoi() {
		return doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}

	public String getIssn() {
		return issn;
	}

	public void setIssn(String issn) {
		this.issn = issn;
	}

	public Academic getOwner() {
		return owner;
	}

	public void setOwner(Academic owner) {
		this.owner = owner;
	}
	
}
