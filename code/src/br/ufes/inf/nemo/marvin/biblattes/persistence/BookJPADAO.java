package br.ufes.inf.nemo.marvin.biblattes.persistence;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.ufes.inf.nemo.marvin.biblattes.domain.Book;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;

@Stateless
public class BookJPADAO extends BaseJPADAO<Book> implements BookDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Class<Book> getDomainClass() {
		return Book.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public Book getBookByTitle(String title) {
		String jpql = "SELECT b FROM Book b where b.title = :title";
		TypedQuery<Book> query = entityManager.createQuery(jpql, Book.class);
		query.setParameter("title", title);
		
		if(query.getResultList().size()>0){
			return query.getResultList().get(0);
		}
		return null;
		
		
	}

}
