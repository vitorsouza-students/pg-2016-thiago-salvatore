package br.ufes.inf.nemo.marvin.biblattes.persistence;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.ufes.inf.nemo.marvin.biblattes.domain.Proceeding;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;

@Stateless
public class ProceedingJPADAO extends BaseJPADAO<Proceeding> implements ProceedingDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Class<Proceeding> getDomainClass() {
		return Proceeding.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Override
	public Proceeding getProceedingByTitle(String title) {
		String jpql = "SELECT a FROM Proceeding a where a.title = :title";
		TypedQuery<Proceeding> query = entityManager.createQuery(jpql, Proceeding.class);
		query.setParameter("title", title);
		
		if(query.getResultList().size()>0){
			return query.getResultList().get(0);
		}
		return null;	
	}
	
	@Override
	public Proceeding getProceedingByDOI(String doi){
		String jpql = "SELECT a FROM Proceeding a where a.doi = :doi";
		TypedQuery<Proceeding> query = entityManager.createQuery(jpql, Proceeding.class);
		query.setParameter("doi", doi);
		
		if(query.getResultList().size()>0){
			return query.getResultList().get(0);
		}
		return null;
	}

}
