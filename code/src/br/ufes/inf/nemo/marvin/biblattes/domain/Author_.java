package br.ufes.inf.nemo.marvin.biblattes.domain;

import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport_;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-06-08T15:27:52.215-0300")
@StaticMetamodel(Author.class)
public class Author_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<Author, String> name;
	public static volatile SingularAttribute<Author, String> sameAs;
}
