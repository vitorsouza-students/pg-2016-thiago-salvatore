package br.ufes.inf.nemo.marvin.biblattes.persistence;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.biblattes.domain.Article;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;

@Local
public interface ArticleDAO extends BaseDAO<Article> {

	public Article getArticleByTitle(String title);

	public Article getArticleByDOI(String doi);

}
