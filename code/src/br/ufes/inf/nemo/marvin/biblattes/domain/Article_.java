package br.ufes.inf.nemo.marvin.biblattes.domain;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-08-12T13:25:38.118-0300")
@StaticMetamodel(Article.class)
public class Article_ extends Publication_ {
	public static volatile SingularAttribute<Article, String> journal;
	public static volatile SingularAttribute<Article, String> volume;
	public static volatile SingularAttribute<Article, String> pages;
	public static volatile SingularAttribute<Article, String> doi;
	public static volatile SingularAttribute<Article, String> issn;
	public static volatile SingularAttribute<Article, Academic> owner;
}
