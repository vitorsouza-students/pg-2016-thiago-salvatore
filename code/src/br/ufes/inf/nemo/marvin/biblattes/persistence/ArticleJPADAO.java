package br.ufes.inf.nemo.marvin.biblattes.persistence;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.ufes.inf.nemo.marvin.biblattes.domain.Article;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;

@Stateless
public class ArticleJPADAO extends BaseJPADAO<Article> implements ArticleDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Class<Article> getDomainClass() {
		return Article.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Override
	public Article getArticleByTitle(String title) {
		String jpql = "SELECT a FROM Article a where a.title = :title";
		TypedQuery<Article> query = entityManager.createQuery(jpql, Article.class);
		query.setParameter("title", title);
		
		if(query.getResultList().size()>0){
			return query.getResultList().get(0);
		}
		return null;
	}
	
	@Override
	public Article getArticleByDOI(String doi){
		String jpql = "SELECT a FROM Article a where a.doi = :doi";
		TypedQuery<Article> query = entityManager.createQuery(jpql, Article.class);
		query.setParameter("doi", doi);
		
		if(query.getResultList().size()>0){
			return query.getResultList().get(0);
		}
		return null;
	}

}
