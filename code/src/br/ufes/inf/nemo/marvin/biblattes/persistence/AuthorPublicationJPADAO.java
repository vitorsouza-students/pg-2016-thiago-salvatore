package br.ufes.inf.nemo.marvin.biblattes.persistence;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.ufes.inf.nemo.marvin.biblattes.domain.AuthorPublication;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;

@Stateless
public class AuthorPublicationJPADAO extends BaseJPADAO<AuthorPublication> implements AuthorPublicationDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Class<AuthorPublication> getDomainClass() {
		return AuthorPublication.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

}
