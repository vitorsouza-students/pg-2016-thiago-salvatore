package br.ufes.inf.nemo.marvin.biblattes.domain;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-08-24T18:23:47.769-0300")
@StaticMetamodel(Collection.class)
public class Collection_ extends Publication_ {
	public static volatile SingularAttribute<Collection, String> bookTitle;
	public static volatile SingularAttribute<Collection, String> publisher;
	public static volatile SingularAttribute<Collection, String> pages;
	public static volatile SingularAttribute<Collection, Academic> owner;
}
