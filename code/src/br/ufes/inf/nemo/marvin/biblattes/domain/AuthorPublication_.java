package br.ufes.inf.nemo.marvin.biblattes.domain;

import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport_;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-08-12T13:31:32.949-0300")
@StaticMetamodel(AuthorPublication.class)
public class AuthorPublication_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<AuthorPublication, Author> author;
	public static volatile SingularAttribute<AuthorPublication, Publication> publication;
}
