package br.ufes.inf.nemo.marvin.biblattes.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.primefaces.model.UploadedFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryParseException;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Resource;
import com.itextpdf.text.pdf.codec.Base64.OutputStream;

import br.ufes.inf.nemo.marvin.biblattes.application.ManageLattesService;
import br.ufes.inf.nemo.marvin.biblattes.domain.Article;
import br.ufes.inf.nemo.marvin.biblattes.domain.Author;
import br.ufes.inf.nemo.marvin.biblattes.domain.AuthorPublication;
import br.ufes.inf.nemo.marvin.biblattes.domain.Book;
import br.ufes.inf.nemo.marvin.biblattes.domain.Collection;
import br.ufes.inf.nemo.marvin.biblattes.domain.Proceeding;
import br.ufes.inf.nemo.marvin.biblattes.domain.Publication;
import br.ufes.inf.nemo.marvin.biblattes.persistence.ArticleDAO;
import br.ufes.inf.nemo.marvin.biblattes.persistence.AuthorDAO;
import br.ufes.inf.nemo.marvin.biblattes.persistence.AuthorPublicationDAO;
import br.ufes.inf.nemo.marvin.biblattes.persistence.BookDAO;
import br.ufes.inf.nemo.marvin.biblattes.persistence.CollectionDAO;
import br.ufes.inf.nemo.marvin.biblattes.persistence.ProceedingDAO;
import br.ufes.inf.nemo.marvin.core.application.SessionInformation;
import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.util.ejb3.controller.JSFController;

@Named
@SessionScoped
public class ManageLattesController extends JSFController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private SessionInformation sessionInformation;
	
	@EJB
	private ArticleDAO articleDAO;
	
	@EJB
	private ProceedingDAO proceedingDAO;
	
	@EJB
	private AuthorDAO authorDAO;
	
	@EJB
	private AuthorPublicationDAO authorPublicationDAO;
	
	@EJB
	private CollectionDAO collectionDAO;
	
	@EJB
	private BookDAO bookDAO;
	
	
	@EJB
	private ManageLattesService manageLattesService;
	
	private List<Publication> publications = new ArrayList<Publication>();
	
	
	private UploadedFile file;
	
	private  String articleQuery = ""
			+ "PREFIX id:   <http://dblp.rkbexplorer.com/id/> "
			+ "PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
			+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "
			+ "PREFIX akt:  <http://www.aktors.org/ontology/portal#> "
			+ "PREFIX owl:  <http://www.w3.org/2002/07/owl#> "
			+ "PREFIX akt:  <http://www.aktors.org/ontology/portal#> "
			+ "PREFIX akts: <http://www.aktors.org/ontology/support#> "
			+ "SELECT DISTINCT ?item ?title ?sData ?author ?authorName WHERE { ?item akt:has-title ?title . "
																	+ "?item akt:has-date ?data . "
																	+ "?data akts:year-of ?sData . "
																	+ "?item akt:has-author ?author . "
																	+ "?author akt:full-name ?authorName ."
																	+ "FILTER (?title = \":name.\") . " 
																	+ "} "
			+ "LIMIT 10";
	
	public void exportBibLattes(){
		manageLattesService.exportBibLattes();
	}
	
	/***
	 * Read the uploaded XML file, and store the bibliography into the database
	 * @param currentUser - The user logged in the system
	 */
	public String readXML(Academic currentUser){
		try{

			InputStream input = file.getInputstream();
			
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(input);
			
			doc.getDocumentElement().normalize();
			
			
			//Fetch all elements under the PRODUCAO-BIBLIOGRAFICA tag
			NodeList nList = doc.getElementsByTagName("PRODUCAO-BIBLIOGRAFICA");
			
			for(int temp=0; temp < nList.getLength(); temp++){
				
				NodeList productions = nList.item(0).getChildNodes();
				
				for(int temp_1=0; temp_1 < productions.getLength(); temp_1++){
					NodeList works = productions.item(temp_1).getChildNodes();
					String typeOfProduction = productions.item(temp_1).getNodeName();
					
					for(int temp_2=0; temp_2 < works.getLength(); temp_2++){
						
						Node node = works.item(temp_2);
						if(node.getNodeType() == Node.ELEMENT_NODE){
							Element eElement = (Element) node;
							
							
							//BibTex @inproceedings
							if(typeOfProduction=="TRABALHOS-EM-EVENTOS"){
								Element dados = (Element) eElement.getElementsByTagName("DADOS-BASICOS-DO-TRABALHO").item(0);	
								NodeList authors = eElement.getElementsByTagName("AUTORES");
								
								Element detalhes = (Element) eElement.getElementsByTagName("DETALHAMENTO-DO-TRABALHO").item(0);
								
								//Check if this entry was found in the DBLP
								Boolean dblpFound = false;
								
								//Check if this entry is the first with this title (multiple authors have multiple entries with same title)
								Boolean isFirst = true;
								
								if(dados.getAttribute("DOI")=="" || proceedingDAO.getProceedingByDOI(dados.getAttribute("DOI"))==null){
									if(proceedingDAO.getProceedingByTitle(dados.getAttribute("TITULO-DO-TRABALHO"))==null){
										//Instantiate a new proceeding object
										Proceeding proceeding = new Proceeding();
																	
										
										String procQuery = articleQuery;
										procQuery = procQuery.replace(":name", dados.getAttribute("TITULO-DO-TRABALHO"));
										
										try {
											
										QueryExecution queryExecution = QueryExecutionFactory.sparqlService("http://dblp.rkbexplorer.com/sparql/", procQuery);
										ResultSet results = queryExecution.execSelect();
										
										while(results.hasNext()) {
											dblpFound = true;
											QuerySolution querySolution = results.next();
											Literal title = querySolution.getLiteral("title");
											Literal date = querySolution.getLiteral("sData");
											Resource item = querySolution.getResource("item");
											Resource author = querySolution.getResource("author");
											Literal authorName = querySolution.getLiteral("authorName");
											
											if(isFirst){
												isFirst = false;
												
												proceeding.setTitle(title.toString());
												proceeding.setYear(date.toString());
												proceeding.setSameAs(item.toString());
												proceeding.setBookTitle(detalhes.getAttribute("TITULO-DOS-ANAIS-OU-PROCEEDINGS"));
												proceeding.setPages(detalhes.getAttribute("PAGINA-INICIAL") + "-" + detalhes.getAttribute("PAGINA-FINAL"));
												proceeding.setOwner(currentUser);
												proceeding.setDoi(dados.getAttribute("DOI"));
												
												proceedingDAO.save(proceeding);
						
											}
											
											AuthorPublication author_publication = new AuthorPublication();
											Author author_db = authorDAO.retrieveAuthorByName(authorName.toString());
											if(author_db != null){
												
												if(author_db.getSameAs()==null){
													author_db.setSameAs(author.toString());
													authorDAO.save(author_db);
												}
												author_publication.setPublication(proceeding);
												author_publication.setAuthor(author_db);
												authorPublicationDAO.save(author_publication);
											}else{
												Author author_dblp = new Author();
												author_dblp.setName(authorName.toString());
												author_dblp.setSameAs(author.toString());
												authorDAO.save(author_dblp);
												
												author_publication.setPublication(proceeding);
												author_publication.setAuthor(author_dblp);
												authorPublicationDAO.save(author_publication);
											}
										}
										
										
										if(!dblpFound){
											
											proceeding.setTitle(dados.getAttribute("TITULO-DO-TRABALHO"));
											proceeding.setYear(dados.getAttribute("ANO-DO-TRABALHO"));
											proceeding.setBookTitle(detalhes.getAttribute("TITULO-DOS-ANAIS-OU-PROCEEDINGS"));
											proceeding.setPages(detalhes.getAttribute("PAGINA-INICIAL") + "-" + detalhes.getAttribute("PAGINA-FINAL"));
											proceeding.setDoi(dados.getAttribute("DOI"));
											proceeding.setOwner(currentUser);
											
											proceedingDAO.save(proceeding);
											
											for(int temp_4=0; temp_4<authors.getLength(); temp_4++){
												AuthorPublication author_publication = new AuthorPublication();
												String author_name = ((Element) authors.item(temp_4)).getAttribute("NOME-COMPLETO-DO-AUTOR");
												Author author_db = authorDAO.retrieveAuthorByName(author_name);
												
												if(author_db != null){
													author_publication.setPublication(proceeding);
													author_publication.setAuthor(author_db);
													authorPublicationDAO.save(author_publication);
												}else{
													Author author_dblp = new Author();
													author_dblp.setName(author_name);
													authorDAO.save(author_dblp);
													
													author_publication.setPublication(proceeding);
													author_publication.setAuthor(author_dblp);
													authorPublicationDAO.save(author_publication);
												}
											}
										}
										}catch(QueryParseException ex) {
											System.out.println(ex.getMessage());
										}
									}else{
										System.out.println("Proceeding with this title already exists");
									}
								}else{
									System.out.println("Proceeding with this DOI already exists");
								}		
							}
							
							//BibTex @article
							if(typeOfProduction=="ARTIGOS-PUBLICADOS"){
								Element dados = (Element) eElement.getElementsByTagName("DADOS-BASICOS-DO-ARTIGO").item(0);
								NodeList authors = eElement.getElementsByTagName("AUTORES");
								Element detalhes = (Element) eElement.getElementsByTagName("DETALHAMENTO-DO-ARTIGO").item(0);
								
								//Check if this entry was found in the DBLP
								Boolean dblpFound = false;
								
								//Check if this entry is the first with this title (multiple authors have multiple entries with same title)
								Boolean isFirst = true;
								

								String aQuery = articleQuery;
								
								aQuery = aQuery.replace(":name", dados.getAttribute("TITULO-DO-ARTIGO"));
								
								QueryExecution queryExecution = QueryExecutionFactory.sparqlService("http://dblp.rkbexplorer.com/sparql/", aQuery);
								
								//There is no article with this title
								if(dados.getAttribute("DOI")=="" || articleDAO.getArticleByDOI(dados.getAttribute("DOI"))==null){
									//There is no article with this doi
									if(articleDAO.getArticleByTitle(dados.getAttribute("TITULO-DO-ARTIGO"))==null){
										
										//This article doesn't exists in our database, so, create a new one
										Article article = new Article();
										
										try {
												
											ResultSet results = queryExecution.execSelect();
											
											//Check if this article exists in the DBLP db
											while(results.hasNext()) {
												dblpFound = true;
												QuerySolution querySolution = results.next();
												
												Literal title = querySolution.getLiteral("title");
												Literal date = querySolution.getLiteral("sData");
												Resource item = querySolution.getResource("item");
												Resource author = querySolution.getResource("author");
												Literal authorName = querySolution.getLiteral("authorName");
												
												
												if(isFirst){
													isFirst = false;
													
													article.setTitle(title.toString());
													article.setYear(date.toString());
													article.setSameAs(item.toString());
													
													article.setJournal(detalhes.getAttribute("TITULO-DO-PERIODICO-OU-REVISTA"));
													article.setVolume(detalhes.getAttribute("VOLUME"));
													article.setPages(detalhes.getAttribute("PAGINA-INICIAL") + "-" + detalhes.getAttribute("PAGINA-FINAL"));
													article.setDoi(dados.getAttribute("DOI"));
													article.setIssn(detalhes.getAttribute("ISSN"));
													article.setOwner(currentUser);
													
													articleDAO.save(article);
												}
												
												
												
												AuthorPublication author_publication = new AuthorPublication();
												Author author_db = authorDAO.retrieveAuthorByName(authorName.toString());
												if(author_db != null){
													
													if(author_db.getSameAs()==null){
														author_db.setSameAs(author.toString());
														authorDAO.save(author_db);
													}
													author_publication.setPublication(article);
													author_publication.setAuthor(author_db);
													authorPublicationDAO.save(author_publication);
												}else{
													Author author_dblp = new Author();
													author_dblp.setName(authorName.toString());
													author_dblp.setSameAs(author.toString());
													authorDAO.save(author_dblp);
													
													author_publication.setPublication(article);
													author_publication.setAuthor(author_dblp);
													authorPublicationDAO.save(author_publication);
												}
											}
											
											if(!dblpFound){
												article.setTitle(dados.getAttribute("TITULO-DO-ARTIGO"));
												article.setJournal(detalhes.getAttribute("TITULO-DO-PERIODICO-OU-REVISTA"));
												article.setVolume(detalhes.getAttribute("VOLUME"));
												article.setPages(detalhes.getAttribute("PAGINA-INICIAL") + "-" + detalhes.getAttribute("PAGINA-FINAL"));
												article.setDoi(dados.getAttribute("DOI"));
												article.setIssn(detalhes.getAttribute("ISSN"));
												article.setYear(dados.getAttribute("ANO-DO-ARTIGO"));
												article.setOwner(currentUser);
												articleDAO.save(article);
	
												for(int temp_3=0; temp_3<authors.getLength(); temp_3++){
													AuthorPublication author_publication = new AuthorPublication();
													String author_name = ((Element) authors.item(temp_3)).getAttribute("NOME-COMPLETO-DO-AUTOR");
													Author author_db = authorDAO.retrieveAuthorByName(author_name);
													
													if(author_db != null){
														author_publication.setPublication(article);
														author_publication.setAuthor(author_db);
														authorPublicationDAO.save(author_publication);
													}else{
														Author author_dblp = new Author();
														author_dblp.setName(author_name);
														authorDAO.save(author_dblp);
														
														author_publication.setPublication(article);
														author_publication.setAuthor(author_dblp);
														authorPublicationDAO.save(author_publication);
													}
												}
											}
										}
										catch(QueryParseException ex){
											System.out.println(ex.getMessage());
										}
									}else{
										//Here we should check what've changed
										System.out.println("DOI already exists");
									}
								}else{
									//Here we should check what've changed
									System.out.println("Title already exists");
								}
							}
							
							if(typeOfProduction=="LIVROS-E-CAPITULOS"){
								//BibTex @book
								if(eElement.getNodeName()=="LIVROS-PUBLICADOS-OU-ORGANIZADOS"){
									NodeList books = eElement.getChildNodes();
									System.out.println("LIVROS PUBLICADOS OU ORGANIADOS");
									for(int temp_5=0; temp_5<books.getLength(); temp_5++){
										Node book = books.item(temp_5);
										
										if(book.getNodeType() == Node.ELEMENT_NODE){
											Element book_e = (Element) book;
											Element dados = (Element) book_e.getElementsByTagName("DADOS-BASICOS-DO-LIVRO").item(0);
											Element details = (Element) book_e.getElementsByTagName("DETALHAMENTO-DO-LIVRO").item(0);
											NodeList authors = book_e.getElementsByTagName("AUTORES");
											
											//Check if this entry was found in the DBLP
											Boolean dblpFound = false;
											
											//Check if this entry is the first with this title (multiple authors have multiple entries with same title)
											Boolean isFirst = true;
											
											if(bookDAO.getBookByTitle(dados.getAttribute("TITULO-DO-LIVRO"))==null){
												//Instantiate a new book
												Book book_published = new Book();
												
												
												String bookQuery = articleQuery;
												bookQuery = bookQuery.replace(":name", dados.getAttribute("TITULO-DO-LIVRO"));
												
												
												try {
													
													QueryExecution queryExecution = QueryExecutionFactory.sparqlService("http://dblp.rkbexplorer.com/sparql/", bookQuery);
													ResultSet results = queryExecution.execSelect();
													
													while(results.hasNext()) {
														
														dblpFound = true;
														QuerySolution querySolution = results.next();
														Literal title = querySolution.getLiteral("title");
														Literal date = querySolution.getLiteral("sData");
														Resource item = querySolution.getResource("item");
														Resource author = querySolution.getResource("author");
														Literal authorName = querySolution.getLiteral("authorName");
														
														if(isFirst){
															isFirst = false;
															
															book_published.setTitle(title.toString());
															book_published.setYear(date.toString());
															book_published.setSameAs(item.toString());
												
															book_published.setEdition(details.getAttribute("NUMERO-DA-EDICAO-REVISAO"));
															book_published.setOwner(currentUser);
															
															bookDAO.save(book_published);
														}
														
														AuthorPublication author_publication = new AuthorPublication();
														Author author_db = authorDAO.retrieveAuthorByName(authorName.toString());
														if(author_db != null){
															
															if(author_db.getSameAs()==null){
																author_db.setSameAs(author.toString());
																authorDAO.save(author_db);
															}
															author_publication.setPublication(book_published);
															author_publication.setAuthor(author_db);
															authorPublicationDAO.save(author_publication);
														}else{
															Author author_dblp = new Author();
															author_dblp.setName(authorName.toString());
															author_dblp.setSameAs(author.toString());
															authorDAO.save(author_dblp);
															
															author_publication.setPublication(book_published);
															author_publication.setAuthor(author_dblp);
															authorPublicationDAO.save(author_publication);
														}
													}
													
													if(!dblpFound){
														book_published.setTitle(dados.getAttribute("TITULO-DO-LIVRO"));
														book_published.setEdition(details.getAttribute("NUMERO-DA-EDICAO-REVISAO"));
														book_published.setYear(dados.getAttribute("ANO"));
														book_published.setOwner(currentUser);
														bookDAO.save(book_published);
														
														for(int temp_3=0; temp_3<authors.getLength(); temp_3++){
															
															AuthorPublication author_publication = new AuthorPublication();
															String author_name = ((Element) authors.item(temp_3)).getAttribute("NOME-COMPLETO-DO-AUTOR");
															Author author_db = authorDAO.retrieveAuthorByName(author_name);
															
															if(author_db != null){
																author_publication.setPublication(book_published);
																author_publication.setAuthor(author_db);
																authorPublicationDAO.save(author_publication);
															}else{
																Author author_dblp = new Author();
																author_dblp.setName(author_name);
																authorDAO.save(author_dblp);
																
																author_publication.setPublication(book_published);
																author_publication.setAuthor(author_dblp);
																authorPublicationDAO.save(author_publication);
															}
														}
													}
												}catch(QueryParseException ex){
													System.out.println(ex.getMessage());
												}
											}else{
												System.out.println("There is a book with this title");
											}
										}
									}
								}
								
								//BibTex @incollection ou @inbook
								if(eElement.getNodeName()=="CAPITULOS-DE-LIVROS-PUBLICADOS"){
									NodeList chapters = eElement.getChildNodes();
									System.out.println("CAPITULOS-DE-LIVROS-PUBLICADOS");
									for(int temp_6=0; temp_6<chapters.getLength(); temp_6++){
										Node chapter = chapters.item(temp_6);
										
										if(chapter.getNodeType() == Node.ELEMENT_NODE){
											Element chapter_e = (Element) chapter;
											Element dados = (Element) chapter_e.getElementsByTagName("DADOS-BASICOS-DO-CAPITULO").item(0);
											Element details = (Element) chapter_e.getElementsByTagName("DETALHAMENTO-DO-CAPITULO").item(0);
											NodeList authors = chapter_e.getElementsByTagName("AUTORES");

											//Check if this entry was found in the DBLP
											Boolean dblpFound = false;
											
											//Check if this entry is the first with this title (multiple authors have multiple entries with same title)
											Boolean isFirst = true;
											
											if(collectionDAO.getCollectionByTitle(dados.getAttribute("TITULO-DO-CAPITULO-DO-LIVRO"))==null){
												//Instantiate a new chapter
												Collection collection = new Collection();
												
												String sectionQuery = articleQuery;
												sectionQuery = sectionQuery.replace(":name", dados.getAttribute("TITULO-DO-CAPITULO-DO-LIVRO"));
												
												QueryExecution queryExecution = QueryExecutionFactory.sparqlService("http://dblp.rkbexplorer.com/sparql/", sectionQuery);
												
												try {
													ResultSet results = queryExecution.execSelect();
													
													while(results.hasNext()) {
														dblpFound = true;
														
														QuerySolution querySolution = results.next();
														Literal title = querySolution.getLiteral("title");
														Literal date = querySolution.getLiteral("sData");
														Resource item = querySolution.getResource("item");
														Resource author = querySolution.getResource("author");
														Literal authorName = querySolution.getLiteral("authorName");
														
														if(isFirst){
															isFirst = false;
															
															collection.setTitle(title.toString());
															collection.setYear(date.toString());
															collection.setSameAs(item.toString());
															
															collection.setBookTitle(details.getAttribute("TITULO-DO-LIVRO"));
															collection.setPages(details.getAttribute("PAGINA-INICIAL") + "-" + details.getAttribute("PAGINA-FINAL"));
															collection.setPublisher(details.getAttribute("NOME-DA-EDITORA"));
															collection.setOwner(currentUser);
															
															collectionDAO.save(collection);
															
														}
														
														AuthorPublication author_publication = new AuthorPublication();
														Author author_db = authorDAO.retrieveAuthorByName(authorName.toString());
														if(author_db != null){
															
															if(author_db.getSameAs()==null){
																author_db.setSameAs(author.toString());
																authorDAO.save(author_db);
															}
															author_publication.setPublication(collection);
															author_publication.setAuthor(author_db);
															authorPublicationDAO.save(author_publication);
														}else{
															Author author_dblp = new Author();
															author_dblp.setName(authorName.toString());
															author_dblp.setSameAs(author.toString());
															authorDAO.save(author_dblp);
															
															author_publication.setPublication(collection);
															author_publication.setAuthor(author_dblp);
															authorPublicationDAO.save(author_publication);
														}
													}
													
													if(!dblpFound){
														collection.setTitle(dados.getAttribute("TITULO-DO-CAPITULO-DO-LIVRO"));
														collection.setYear(dados.getAttribute("ANO"));
														collection.setBookTitle(details.getAttribute("TITULO-DO-LIVRO"));
														collection.setPages(details.getAttribute("PAGINA-INICIAL") + "-" + details.getAttribute("PAGINA-FINAL"));
														collection.setPublisher(details.getAttribute("NOME-DA-EDITORA"));
														collection.setOwner(currentUser);
														
														collectionDAO.save(collection);
														
														for(int temp_3=0; temp_3<authors.getLength(); temp_3++){
															AuthorPublication author_publication = new AuthorPublication();
															String author_name = ((Element) authors.item(temp_3)).getAttribute("NOME-COMPLETO-DO-AUTOR");
															Author author_db = authorDAO.retrieveAuthorByName(author_name);
															
															if(author_db != null){
																author_publication.setPublication(collection);
																author_publication.setAuthor(author_db);
																authorPublicationDAO.save(author_publication);
															}else{
																Author author_dblp = new Author();
																author_dblp.setName(author_name);
																authorDAO.save(author_dblp);
																
																author_publication.setPublication(collection);
																author_publication.setAuthor(author_dblp);
																authorPublicationDAO.save(author_publication);
															}
														}
													}
												}catch(QueryParseException ex){
													System.out.println(ex.getMessage());
												}
											}else{
												System.out.println("There is a chapter with this title");
											}
										}
									}
								}
							}
						}
					}
				}
				
			}
			return "curriculo.xhtml?faces-redirect=true";
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}
	
	
	public List<Article> getArticles() {
		return articleDAO.retrieveAll();

	}

	public List<Book> getBooks() {
		return bookDAO.retrieveAll();
	}
	
	public List<Collection> getCollections() {
		return collectionDAO.retrieveAll();
	}
	
	public List<Proceeding> getProceedings() {
		
		return proceedingDAO.retrieveAll();

	}
	
	public List<Publication> getAllPublications() {
		publications.addAll(articleDAO.retrieveAll());
		publications.addAll(bookDAO.retrieveAll());
		publications.addAll(collectionDAO.retrieveAll());
		publications.addAll(proceedingDAO.retrieveAll());
		
		return publications;
	}
}