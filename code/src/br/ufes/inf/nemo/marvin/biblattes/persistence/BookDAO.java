package br.ufes.inf.nemo.marvin.biblattes.persistence;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.biblattes.domain.Book;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;

@Local
public interface BookDAO extends BaseDAO<Book> {

	public Book getBookByTitle(String title);

}
