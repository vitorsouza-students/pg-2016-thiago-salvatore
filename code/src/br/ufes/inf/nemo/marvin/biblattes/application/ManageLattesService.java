package br.ufes.inf.nemo.marvin.biblattes.application;

import java.io.Serializable;

import javax.ejb.Local;


@Local
public interface ManageLattesService extends Serializable {

	public void exportBibLattes();

}
