package br.ufes.inf.nemo.marvin.biblattes.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;

@Entity
public class Collection extends Publication {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String bookTitle;
	
	private String publisher;
	
	private String pages;
	
	@ManyToOne
	@NotNull
	private Academic owner;

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Academic getOwner() {
		return owner;
	}

	public void setOwner(Academic owner) {
		this.owner = owner;
	}

	public String getPages() {
		return pages;
	}

	public void setPages(String pages) {
		this.pages = pages;
	}

	
	
}
