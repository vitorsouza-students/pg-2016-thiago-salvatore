package br.ufes.inf.nemo.marvin.biblattes.persistence;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.ufes.inf.nemo.marvin.biblattes.domain.Collection;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;

@Stateless
public class CollectionJPADAO extends BaseJPADAO<Collection> implements CollectionDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Class<Collection> getDomainClass() {
		return Collection.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public Collection getCollectionByTitle(String title) {
		String jpql = "SELECT c FROM Collection c where c.title = :title";
		TypedQuery<Collection> query = entityManager.createQuery(jpql, Collection.class);
		query.setParameter("title", title);
		
		if(query.getResultList().size()>0){
			return query.getResultList().get(0);
		}
		return null;
		
		
	}

}
