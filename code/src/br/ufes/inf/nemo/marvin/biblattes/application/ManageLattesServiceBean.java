package br.ufes.inf.nemo.marvin.biblattes.application;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import br.ufes.inf.nemo.marvin.biblattes.domain.Article;
import br.ufes.inf.nemo.marvin.biblattes.domain.Author;
import br.ufes.inf.nemo.marvin.biblattes.domain.AuthorPublication;
import br.ufes.inf.nemo.marvin.biblattes.domain.Book;
import br.ufes.inf.nemo.marvin.biblattes.domain.Collection;
import br.ufes.inf.nemo.marvin.biblattes.domain.Proceeding;
import br.ufes.inf.nemo.marvin.biblattes.persistence.ArticleDAO;
import br.ufes.inf.nemo.marvin.biblattes.persistence.AuthorDAO;
import br.ufes.inf.nemo.marvin.biblattes.persistence.AuthorPublicationDAO;
import br.ufes.inf.nemo.marvin.biblattes.persistence.BookDAO;
import br.ufes.inf.nemo.marvin.biblattes.persistence.CollectionDAO;
import br.ufes.inf.nemo.marvin.biblattes.persistence.ProceedingDAO;
import br.ufes.inf.nemo.marvin.core.application.SessionInformation;

@Stateless
public class ManageLattesServiceBean implements ManageLattesService {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageLattesServiceBean.class.getCanonicalName());
	
	
	@EJB
	private SessionInformation sessionInformation;
	
	@EJB
	private ArticleDAO articleDAO;
	
	@EJB
	private ProceedingDAO proceedingDAO;
	
	@EJB
	private AuthorDAO authorDAO;
	
	@EJB
	private AuthorPublicationDAO authorPublicationDAO;
	
	@EJB
	private CollectionDAO collectionDAO;
	
	@EJB
	private BookDAO bookDAO;
	

	private String generateAuthorsText(List<AuthorPublication> authors){
		
		String authorsText = "";
		for(AuthorPublication author : authors){
			Author auth = author.getAuthor();
			authorsText = authorsText + "author = {" + auth.getName() + "},\n";
		}
		return authorsText;
	}
	
	private void writeBookToFile(List<Book> books, java.io.OutputStream output){

		for(Book book : books){
			
			List<AuthorPublication> authors = book.getAuthors();
			String authorText = generateAuthorsText(authors);
			String bibTexString = "@book{"+book.getId()+",\n"
					+ authorText
					+ "title = {" + book.getTitle()+"},\n"
					+ "year = {" + book.getYear()+"},\n"
					+ "edition = {" + book.getEdition() + "}}\n\n\n";
			
			try {
				output.write(bibTexString.getBytes());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private void writeArticleToFile(List<Article> articles, java.io.OutputStream output){

		for(Article article : articles){
			
			List<AuthorPublication> authors = article.getAuthors();
			String authorText = generateAuthorsText(authors);
			String bibTexString = "@article{"+article.getId()+",\n"
					+ authorText
					+ "title = {" + article.getTitle()+"},\n"
					+ "journal= {" + article.getJournal()+"},\n"
					+ "pages = {" + article.getPages()+"},\n"
					+ "doi = {" + article.getDoi()+"},\n"
					+ "isbn = {" + article.getIssn()+"},\n"
					+ "year = {" + article.getYear()+"},\n"
					+ "volume = {" + article.getVolume() + "}}\n\n\n";
			
			try {
				output.write(bibTexString.getBytes(Charset.forName("UTF-8")));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private void writeProceedingToFile(List<Proceeding> proceedings, java.io.OutputStream output){

		for(Proceeding proceeding : proceedings){
			
			List<AuthorPublication> authors = proceeding.getAuthors();
			String authorText = generateAuthorsText(authors);
			String bibTexString = "@inproceedings{"+proceeding.getId()+",\n"
					+ authorText
					+ "title = {" + proceeding.getTitle()+"},\n"
					+ "booktitle = {" + proceeding.getBookTitle()+"},\n"
					+ "pages = {" + proceeding.getPages()+"},\n"
					+ "doi = {" + proceeding.getDoi()+"},\n"
					+ "year = {" + proceeding.getYear()+"}}\n\n\n";
			
			try {
				output.write(bibTexString.getBytes(Charset.forName("UTF-8")));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private void writeCollectionToFile(List<Collection> collections, java.io.OutputStream output){

		for(Collection collection : collections){
			
			List<AuthorPublication> authors = collection.getAuthors();
			String authorText = generateAuthorsText(authors);
			String bibTexString = "@incollection{"+collection.getId()+",\n"
					+ authorText
					+ "title = {" + collection.getTitle()+"},\n"
					+ "booktitle = {" + collection.getBookTitle()+"},\n"
					+ "publisher = {" + collection.getPublisher()+"},\n"
					+ "pages = {" + collection.getPages()+"},\n"
					+ "year = {" + collection.getYear()+"}}\n\n\n";
			
			try {
				output.write(bibTexString.getBytes(Charset.forName("UTF-8")));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public void exportBibLattes(){
		
		
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		ec.setResponseHeader("Content-type", "application/x-bibtex");
		ec.setResponseHeader("Content-Disposition", "attachment; filename=publications.bib");
		
		try {
			java.io.OutputStream output = ec.getResponseOutputStream();
			
			List<Article> articles = articleDAO.retrieveAll();
			List<Proceeding> proceedings = proceedingDAO.retrieveAll();
			List<Book> books = bookDAO.retrieveAll();
			List<Collection> collections = collectionDAO.retrieveAll();
			
			writeBookToFile(books, output);
			writeArticleToFile(articles, output);
			writeProceedingToFile(proceedings, output);
			writeCollectionToFile(collections, output);
			output.flush();
			output.close();
			FacesContext.getCurrentInstance().responseComplete();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	

}
