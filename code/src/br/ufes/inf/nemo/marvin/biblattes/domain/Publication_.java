package br.ufes.inf.nemo.marvin.biblattes.domain;

import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport_;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-08-12T14:04:17.598-0300")
@StaticMetamodel(Publication.class)
public class Publication_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<Publication, String> title;
	public static volatile SingularAttribute<Publication, String> year;
	public static volatile SingularAttribute<Publication, String> sameAs;
	public static volatile ListAttribute<Publication, AuthorPublication> authors;
}
