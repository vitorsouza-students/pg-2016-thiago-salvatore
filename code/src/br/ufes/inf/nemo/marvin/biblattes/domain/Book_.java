package br.ufes.inf.nemo.marvin.biblattes.domain;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-08-12T13:29:44.655-0300")
@StaticMetamodel(Book.class)
public class Book_ extends Publication_ {
	public static volatile SingularAttribute<Book, String> edition;
	public static volatile SingularAttribute<Book, Academic> owner;
}
