package br.ufes.inf.nemo.marvin.people.domain;

import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport_;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-03-11T12:28:07.148-0300")
@StaticMetamodel(ContactType.class)
public class ContactType_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<ContactType, String> type;
}
