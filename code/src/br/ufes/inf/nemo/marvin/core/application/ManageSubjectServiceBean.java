package br.ufes.inf.nemo.marvin.core.application;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.marvin.core.domain.Subject;
import br.ufes.inf.nemo.marvin.core.persistence.SubjectDAO;
import br.ufes.inf.nemo.util.ejb3.application.CrudException;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.PersistentObjectNotFoundException;

@Stateless
public class ManageSubjectServiceBean extends CrudServiceBean<Subject> implements ManageSubjectService {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;

	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageSubjectServiceBean.class.getCanonicalName());
	
	
	@EJB
	private SubjectDAO subjectDAO;
	
	@Override
	public BaseDAO<Subject> getDAO() {
		return subjectDAO;
	}

	@Override
	protected Subject createNewEntity() {
		return new Subject();
	}
	
	public List<Subject> listSubjects() {
		return subjectDAO.retrieveAll();
	}
	@Override
	public void validateCreate(Subject entity) throws CrudException {
		// Possibly throwing a CRUD Exception to indicate validation error.
		CrudException crudException = null;
		String crudExceptionMessage = "The subject \"" + entity.getCode() + "\" cannot be created due to validation errors.";
		
		//Business rules
		//Rule 1 - Cannot have two subjects with the same code
		try {
			List<Subject> subjects = subjectDAO.retrieveByCode(entity.getCode());
			if(!subjects.isEmpty()){
				logger.log(Level.INFO, "Creation of subject \"{0}\" violates validation rule 1: subject with id {2} has same code", new Object[] { entity.getCode(), subjects.get(0).getId() });
				crudException = addValidationError(crudException, crudExceptionMessage, "manageSubject.error.create.same_code");
			}
		}
		catch(PersistentObjectNotFoundException e){
			logger.log(Level.FINE, "Rule 1 OK - there`s no other subject with the same code: {0}", new Object[] {entity.getCode()});
		}
		catch (MultiplePersistentObjectsFoundException e){
			logger.log(Level.WARNING, "Creation of subject \"" + entity.getCode() + "\" threw an exception: a query for subject with this code returned multiple results!", e);
			crudException = addValidationError(crudException, crudExceptionMessage, "same_code", "installSystem.button.registerAdministrator");
		}
		if(crudException != null)
			throw crudException;
	}

	@Override
	public void validateUpdate(Subject entity) throws CrudException {
		// Possibly throwing a CRUD Exception to indicate validation error.
		CrudException crudException = null;
		String crudExceptionMessage = "The subject \"" + entity.getCode() + "\" cannot be updated due to validation errors.";
		
		//Business rules
		//Rule 1 - Cannot have two subjects with the same code
		try {
			List<Subject> subjects = subjectDAO.retrieveByCode(entity.getCode());
			if(!subjects.isEmpty()){
				if(subjects.get(0).getId()!=entity.getId()){
					logger.log(Level.INFO, "Update of subject \"{0}\" violates validation rule 1: subject with id {2} has same code", new Object[] { entity.getCode(), subjects.get(0).getId() });
					crudException = addValidationError(crudException, crudExceptionMessage, "manageSubject.error.create.same_code");	
				}
			}
		}
		catch(PersistentObjectNotFoundException e){
			logger.log(Level.FINE, "Rule 1 OK - there`s no other subject with the same code: {0}", new Object[] {entity.getCode()});
		}
		catch (MultiplePersistentObjectsFoundException e){
			logger.log(Level.WARNING, "Update of subject \"" + entity.getCode() + "\" threw an exception: a query for subject with this code returned multiple results!", e);
			crudException = addValidationError(crudException, crudExceptionMessage, "same_code", "installSystem.button.registerAdministrator");
		}
		if(crudException != null)
			throw crudException;
	}

}
