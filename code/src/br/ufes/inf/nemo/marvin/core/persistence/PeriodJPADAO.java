package br.ufes.inf.nemo.marvin.core.persistence;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.PersistentObjectNotFoundException;

@Stateless
public class PeriodJPADAO extends BaseJPADAO<Period> implements PeriodDAO {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;

	/** The logger. */
	private static final Logger logger = Logger.getLogger(PeriodJPADAO.class.getCanonicalName());

	/** The application's persistent context provided by the application server. */
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Class<Period> getDomainClass() {
		return Period.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public List<Period> retriveByYearNumber(int year, int number)  throws PersistentObjectNotFoundException, MultiplePersistentObjectsFoundException {
		String jpql = "select p from Period p where p.year = :year and p.number = :number";
		TypedQuery<Period> query = entityManager.createQuery(jpql, Period.class);
		query.setParameter("year", year);
		query.setParameter("number", number);
		return query.getResultList();
	}

}
