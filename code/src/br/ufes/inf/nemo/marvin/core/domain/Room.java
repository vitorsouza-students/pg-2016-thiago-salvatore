package br.ufes.inf.nemo.marvin.core.domain;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.marvin.alocaweb.domain.Allocation;
import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;

@Entity
public class Room extends PersistentObjectSupport implements Comparable<Room> {
	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	@Basic
	@NotNull
	private String buildingCode;
	
	@Basic
	@NotNull
	private int buildingFloor;
	
	@Basic
	@NotNull
	private int number;
	
	@Basic
	@NotNull
	private int slots;
	
	@OneToMany(mappedBy="room", cascade = CascadeType.ALL)
	private List<Allocation> allocations;

	public String getBuildingCode() {
		return buildingCode;
	}

	public void setBuildingCode(String buildingCode) {
		this.buildingCode = buildingCode;
	}

	public int getBuildingFloor() {
		return buildingFloor;
	}

	public void setBuildingFloor(int buildingFloor) {
		this.buildingFloor = buildingFloor;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getSlots() {
		return slots;
	}

	public void setSlots(int slots) {
		this.slots = slots;
	}
	
	@Override
	public int compareTo(Room o) {
		return uuid.compareTo(o.uuid);
	}

}
