package br.ufes.inf.nemo.marvin.core.domain;

import br.ufes.inf.nemo.marvin.alocaweb.domain.Allocation;
import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport_;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-04T15:27:37.272-0300")
@StaticMetamodel(Class.class)
public class Class_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<Class, String> number;
	public static volatile SingularAttribute<Class, Integer> slots;
	public static volatile SingularAttribute<Class, Academic> academic;
	public static volatile SingularAttribute<Class, Period> period;
	public static volatile SingularAttribute<Class, Course> course;
	public static volatile SingularAttribute<Class, Subject> subject;
	public static volatile ListAttribute<Class, Allocation> allocations;
}
