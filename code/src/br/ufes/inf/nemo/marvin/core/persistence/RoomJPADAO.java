package br.ufes.inf.nemo.marvin.core.persistence;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.ufes.inf.nemo.marvin.alocaweb.domain.Allocation;
import br.ufes.inf.nemo.marvin.core.domain.Room;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.PersistentObjectNotFoundException;

@Stateless
public class RoomJPADAO extends BaseJPADAO<Room> implements RoomDAO {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;

	/** The logger. */
	private static final Logger logger = Logger.getLogger(RoomJPADAO.class.getCanonicalName());

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Class<Room> getDomainClass() {
		return Room.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public List<Room> retrieveByNumberBuildingFloor(int number, String bCode, int bFloor) throws PersistentObjectNotFoundException, MultiplePersistentObjectsFoundException {
		String jpql = "select r from Room r where r.number = :number and r.buildingCode = :bCode and r.buildingFloor = :bFloor";
		TypedQuery<Room> query = entityManager.createQuery(jpql, Room.class);
		query.setParameter("number", number);
		query.setParameter("bCode", bCode);
		query.setParameter("bFloor", bFloor);
		
		return query.getResultList();
	}

	@Override
	public List<Allocation> retrieveAllocationsByRoom(Room entity) {
		String jpql = "select a from Allocation a where a.room = :room and a.class_.slots > :slots";
		TypedQuery<Allocation> query = entityManager.createQuery(jpql, Allocation.class);
		query.setParameter("room", entity);
		query.setParameter("slots", entity.getSlots());
		return query.getResultList();
	}

}
