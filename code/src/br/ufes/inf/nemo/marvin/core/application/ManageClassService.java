package br.ufes.inf.nemo.marvin.core.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.core.domain.Class;
import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.marvin.core.domain.Subject;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;

@Local
public interface ManageClassService extends CrudService<Class> {
	public List<Class> retrieveClassByPeriod(Period period);
	public List<Class> listClasses();
	public List<Class> getClasses();
	public void setClasses(List<Class> classes);
	public List<Class> retrieveClassByCourse(Course selectedEntity);
	public List<Class> retrieveClassBySubject(Subject selectedEntity);
}
