package br.ufes.inf.nemo.marvin.core.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.core.application.ManageRoomService;
import br.ufes.inf.nemo.marvin.core.domain.Class;
import br.ufes.inf.nemo.marvin.core.domain.Room;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;

@Named
@SessionScoped
public class ManageRoomController extends CrudController<Room> {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	private List<Room> rooms;
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageAcademicController.class.getCanonicalName());
	
	@EJB
	private ManageRoomService manageRoomService;
	
	public ManageRoomController() {
		 viewPath = "/core/manageRoom/";
	     bundleName = "msgsCore";
	}
	
	@PostConstruct
	public void init(){
		rooms = manageRoomService.listRooms();
	}
	
	@Override
	protected CrudService<Room> getCrudService() {
		return manageRoomService;
	}

	@Override
	protected Room createNewEntity() {
		logger.log(Level.INFO, "INITIALIZING AN EMPTY Room.");
		return new Room();
	}

	@Override
	protected void initFilters() {
		// TODO Auto-generated method stub
		
	}

	public String createEntity(){
		if(selectedEntity.getId()==null){
			rooms.add(selectedEntity);
		}
		return save();
	}

	public String deleteEntity(){
		rooms.remove(selectedEntity);
		return delete();
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}
	

	public void updateRooms(ValueChangeEvent event){
		
		List<Room> all_rooms = manageRoomService.listRooms();
		Class newClass = (Class) event.getNewValue();
		
		rooms = all_rooms.stream()
				.filter(r -> r.getSlots() >= newClass.getSlots()).collect(Collectors.toList());
	}
}
