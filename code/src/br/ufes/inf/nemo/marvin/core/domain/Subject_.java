package br.ufes.inf.nemo.marvin.core.domain;

import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport_;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-17T18:18:14.175-0300")
@StaticMetamodel(Subject.class)
public class Subject_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<Subject, String> name;
	public static volatile SingularAttribute<Subject, String> code;
	public static volatile ListAttribute<Subject, Class> classes;
}
