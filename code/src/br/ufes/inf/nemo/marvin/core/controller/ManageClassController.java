package br.ufes.inf.nemo.marvin.core.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.core.application.ManageClassService;
import br.ufes.inf.nemo.marvin.core.application.ManagePeriodService;
import br.ufes.inf.nemo.marvin.core.domain.Class;
import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.application.filters.LikeFilter;
import br.ufes.inf.nemo.util.ejb3.application.filters.MultipleChoiceFilter;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;

@Named
@SessionScoped
public class ManageClassController extends CrudController<br.ufes.inf.nemo.marvin.core.domain.Class> {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageAcademicController.class.getCanonicalName());
	
	@EJB
	private ManageClassService manageClassService;
	
	@EJB
	private ManagePeriodService managePeriodService;
	
	
	public ManageClassController() {
		 viewPath = "/core/manageClass/";
	     bundleName = "msgsCore";
	}
	
	@Override
	protected CrudService<Class> getCrudService() {
		return manageClassService;
	}

	@Override
	protected Class createNewEntity() {
		return new Class();
	}

	@Override
	protected void initFilters() {
		logger.log(Level.FINE, "Initializing filter types");

		List<Period> periods = managePeriodService.getPeriods();
		Map<String, String> labels = new LinkedHashMap<String, String>();
		for(Period period : periods)
			labels.put(period.getId().toString(), Integer.toString(period.getYear())+"/"+Integer.toString(period.getNumber()));
		addFilter(new MultipleChoiceFilter<Period>("manageClass.filter.byPeriod", "period", "Period", periods, labels));
	
		addFilter(new LikeFilter("manageClass.filter.byAcademic", "academic.name", "Professor"));
		addFilter(new LikeFilter("manageClass.filter.bySubject", "subject.name", "Subject"));

	}
	
	public List<Class> getClasses() {
		return manageClassService.getClasses();
	}

	public void setClasses(List<Class> classes) {
		manageClassService.setClasses(classes);
	}

	public List<Class> retrieveByPeriod(Long id){
		logger.log(Level.INFO, "Fetching classes by period");
		return manageClassService.retrieveClassByPeriod(null);
	}

	public String createEntity(){
		if(selectedEntity.getId()==null){
			manageClassService.getClasses().add(selectedEntity);
		}
		return save();
	}

	public String deleteEntity(){
		manageClassService.getClasses().remove(selectedEntity);
		return delete();
	}

}
