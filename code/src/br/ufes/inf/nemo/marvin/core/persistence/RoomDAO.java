package br.ufes.inf.nemo.marvin.core.persistence;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.alocaweb.domain.Allocation;
import br.ufes.inf.nemo.marvin.core.domain.Room;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.PersistentObjectNotFoundException;

@Local
public interface RoomDAO extends BaseDAO<Room> {

	public List<Room> retrieveByNumberBuildingFloor(int number, String bCode, int bFloor) throws PersistentObjectNotFoundException, MultiplePersistentObjectsFoundException;

	public List<Allocation> retrieveAllocationsByRoom(Room entity);
}
