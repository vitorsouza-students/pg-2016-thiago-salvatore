package br.ufes.inf.nemo.marvin.core.persistence;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.ufes.inf.nemo.marvin.core.domain.Subject;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.PersistentObjectNotFoundException;


@Stateless
public class SubjectJPADAO extends BaseJPADAO<Subject> implements SubjectDAO {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;

	/** The logger. */
	private static final Logger logger = Logger.getLogger(AcademicJPADAO.class.getCanonicalName());

	/** The application's persistent context provided by the application server. */
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Class<Subject> getDomainClass() {
		return Subject.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public List<Subject> retrieveByCode(String code) throws PersistentObjectNotFoundException, MultiplePersistentObjectsFoundException {
		String jpql = "select s from Subject s where s.code = :code";
		TypedQuery<Subject> query = entityManager.createQuery(jpql, Subject.class);
		query.setParameter("code", code);
		return query.getResultList();
	}

}
