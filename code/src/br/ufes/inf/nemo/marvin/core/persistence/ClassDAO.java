package br.ufes.inf.nemo.marvin.core.persistence;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.core.domain.Class;
import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.marvin.core.domain.Subject;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.PersistentObjectNotFoundException;

@Local
public interface ClassDAO extends BaseDAO<br.ufes.inf.nemo.marvin.core.domain.Class>{
	public List<br.ufes.inf.nemo.marvin.core.domain.Class> retrieveClassByPeriod(Period period);
	public List<Class> retrieveClassByNumberPeriod(String number, Period period) throws PersistentObjectNotFoundException, MultiplePersistentObjectsFoundException;
	public List<Class> retrieveClassByCourse(Course selectedEntity);
	public List<Class> retrieveClassBySubject(Subject selectedEntity);
}
