package br.ufes.inf.nemo.marvin.core.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.core.application.ManageClassService;
import br.ufes.inf.nemo.marvin.core.application.ManageSubjectService;
import br.ufes.inf.nemo.marvin.core.domain.Class;
import br.ufes.inf.nemo.marvin.core.domain.Subject;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;

@Named
@SessionScoped
public class ManageSubjectController extends CrudController<Subject> {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	private List<Subject> subjects;
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageAcademicController.class.getCanonicalName());


	@EJB
	private ManageSubjectService manageSubjectService;
	
	@EJB
	private ManageClassService manageClassService;
	
	
	public ManageSubjectController() {
		 viewPath = "/core/manageSubject/";
	     bundleName = "msgsCore";
	}
	
	@PostConstruct
	public void init(){
		subjects = manageSubjectService.listSubjects();
	}
	
	@Override
	protected CrudService<Subject> getCrudService() {
		return manageSubjectService;
	}

	@Override
	protected Subject createNewEntity() {
		logger.log(Level.INFO, "INITIALIZING AN EMPTY Subject.");
		return new Subject();
	}

	@Override
	protected void initFilters() {
		// TODO Auto-generated method stub
		
	}
	
	public String createEntity(){
		if(selectedEntity.getId()==null){
			subjects.add(selectedEntity);
		}
		return save();
	}

	public String deleteEntity(){
		subjects.remove(selectedEntity);
		
		List<Class> classes = manageClassService.retrieveClassBySubject(selectedEntity);
		
		for(Class c : classes){
			manageClassService.getClasses().remove(c);
		}
		
		return delete();
	}

	public List<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}

	
}
