package br.ufes.inf.nemo.marvin.core.controller;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.core.application.ManageClassService;
import br.ufes.inf.nemo.marvin.core.application.ManageCourseService;
import br.ufes.inf.nemo.marvin.core.domain.Class;
import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;

@Named
@SessionScoped
public class ManageCourseController extends CrudController<Course> {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	private List<Course> courses;
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageCourseController.class.getCanonicalName());
	
	@EJB
	private ManageCourseService manageCourseService;
	
	@EJB
	private ManageClassService manageClassService;
	
	public ManageCourseController() {
		 viewPath = "/core/manageCourse/";
	     bundleName = "msgsCore";
	}
	
	@PostConstruct
	public void init(){
		courses = manageCourseService.listCourses();
	}
	@Override
	protected CrudService<Course> getCrudService() {
		return manageCourseService;
	}

	@Override
	protected Course createNewEntity() {
		return new Course();
	}

	@Override
	protected void initFilters() {
		// TODO Auto-generated method stub
		
	}

	public String createEntity(){
		if(selectedEntity.getId()==null){
			courses.add(selectedEntity);
		}
		return save();
	}
	
	public String deleteEntity(){
		courses.remove(selectedEntity);
		
		List<Class> classes = manageClassService.retrieveClassByCourse(selectedEntity);
		
		for(Class c : classes){
			manageClassService.getClasses().remove(c);
		}
		return delete();
	}
	
	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	
}
