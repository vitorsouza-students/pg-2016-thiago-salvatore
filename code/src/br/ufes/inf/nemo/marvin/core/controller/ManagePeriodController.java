package br.ufes.inf.nemo.marvin.core.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.alocaweb.application.ManageAllocationService;
import br.ufes.inf.nemo.marvin.core.application.ManageClassService;
import br.ufes.inf.nemo.marvin.core.application.ManagePeriodService;
import br.ufes.inf.nemo.marvin.core.domain.Class;
import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;

@Named
@SessionScoped
public class ManagePeriodController extends CrudController<Period> {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageAcademicController.class.getCanonicalName());
	
	@EJB
	private ManagePeriodService managePeriodService;
	
	@EJB
	private ManageClassService manageClassService;
	
	@EJB
	private ManageAllocationService manageAllocationService;
	
	public ManagePeriodController() {
		 viewPath = "/core/managePeriod/";
	     bundleName = "msgsCore";
	}
	
	@Override
	protected CrudService<Period> getCrudService() {
		return managePeriodService;
	}

	@Override
	protected Period createNewEntity() {
		logger.log(Level.INFO, "INITIALIZING AN EMPTY Period.");
		return new Period();
	}

	@Override
	protected void initFilters() {
		// TODO Auto-generated method stub
		
	}

	public String createEntity(){
		if(selectedEntity.getId()==null){
			managePeriodService.getPeriods().add(selectedEntity);
		}
		return save();
	}
	
	public String deleteEntity(){
		managePeriodService.getPeriods().remove(selectedEntity);
		
		List<Class> classes = manageClassService.retrieveClassByPeriod(selectedEntity);
		
		for(Class c : classes) {
			manageClassService.getClasses().remove(c);
		}
		
		return delete();
	}
	
	public List<Period> getPeriods() {
		return managePeriodService.getPeriods();
	}

	public void setPeriods(List<Period> periods) {
		managePeriodService.setPeriods(periods);
	}

	
}
