package br.ufes.inf.nemo.marvin.core.application;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.marvin.core.persistence.PeriodDAO;
import br.ufes.inf.nemo.util.ejb3.application.CrudException;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.PersistentObjectNotFoundException;

@Stateless
public class ManagePeriodServiceBean extends CrudServiceBean<Period> implements ManagePeriodService {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	private List<Period> periods;
	
	@EJB
	private PeriodDAO periodDAO;
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManagePeriodServiceBean.class.getCanonicalName());
	
	@PostConstruct
	public void init(){
		this.periods = periodDAO.retrieveAll();
	}
	@Override
	public BaseDAO<Period> getDAO() {
		return periodDAO;
	}

	@Override
	protected Period createNewEntity() {
		return new Period();
	}

	@Override
	public void validateCreate(Period entity) throws CrudException {
		// Possibly throwing a CRUD Exception to indicate validation error.
		CrudException crudException = null;
		String crudExceptionMessage = "The period \"" + entity.getYear() +"/"+ entity.getNumber() + "\" cannot be created due to validation errors.";
		
		//Business rules
		//Rule 1 - Cannot have two periods with the same year and number
		try {
			List<Period> period = periodDAO.retriveByYearNumber(entity.getYear(), entity.getNumber());
			if(!period.isEmpty()){
				logger.log(Level.INFO, "Creation of period \"{0}\"\\\"{1}\" violates validation rule 1: period with id {2} has same year and number", new Object[] { entity.getYear(), entity.getNumber(), period.get(0).getId() });
				crudException = addValidationError(crudException, crudExceptionMessage, "managePeriod.error.create.same_year_number");
			}
		}
		catch(PersistentObjectNotFoundException e){
			logger.log(Level.FINE, "Rule 1 OK - there`s no other period with the same number and year: {0}/{1}", new Object[] {entity.getYear(), entity.getNumber()});
		}
		catch (MultiplePersistentObjectsFoundException e){
			logger.log(Level.WARNING, "Creation of period \"" + entity.getYear() + "/" + entity.getNumber() + "\" threw an exception: a query for institution with this name returned multiple results!", e);
			crudException = addValidationError(crudException, crudExceptionMessage, "number_year", "installSystem.button.registerAdministrator");
		}
		if(crudException != null)
			throw crudException;
	}

	@Override
	public void validateUpdate(Period entity) throws CrudException {
		// Possibly throwing a CRUD Exception to indicate validation error.
		CrudException crudException = null;
		String crudExceptionMessage = "The period \"" + entity.getYear() +"/"+ entity.getNumber() + "\" cannot be updated due to validation errors.";
		
		//Business rules
		//Rule 1 - Cannot have two periods with the same year and number
		try {
			List<Period> period = periodDAO.retriveByYearNumber(entity.getYear(), entity.getNumber());
			if(!period.isEmpty()){
				if(period.get(0).getId()!=entity.getId()){
					logger.log(Level.INFO, "Update of period \"{0}\"\\\"{1}\" violates validation rule 1: period with id {2} has same year and number", new Object[] { entity.getYear(), entity.getNumber(), period.get(0).getId() });
					crudException = addValidationError(crudException, crudExceptionMessage, "managePeriod.error.create.same_year_number");	
				}
			}
		}
		catch(PersistentObjectNotFoundException e){
			logger.log(Level.FINE, "Rule 1 OK - there`s no other period with the same number and year: {0}/{1}", new Object[] {entity.getYear(), entity.getNumber()});
		}
		catch (MultiplePersistentObjectsFoundException e){
			logger.log(Level.WARNING, "Update of period \"" + entity.getYear() + "/" + entity.getNumber() + "\" threw an exception: a query for institution with this name returned multiple results!", e);
			crudException = addValidationError(crudException, crudExceptionMessage, "number_year", "installSystem.button.registerAdministrator");
		}
		if(crudException != null)
			throw crudException;
	}

	public List<Period> getPeriods() {
		return periods;
	}

	public void setPeriods(List<Period> periods) {
		this.periods = periods;
	}

}
