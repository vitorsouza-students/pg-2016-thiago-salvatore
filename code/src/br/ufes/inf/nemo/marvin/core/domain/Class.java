package br.ufes.inf.nemo.marvin.core.domain;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.marvin.alocaweb.domain.Allocation;
import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;

@Entity
public class Class extends PersistentObjectSupport implements Comparable<Class> {
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** Code of this class */
	@Basic
	@NotNull
	private String number;
	
	/** Number of slots available for this class */
	@Basic
	@NotNull
	private int slots;
	
	
	/** A class has a professor that will teach */
	@ManyToOne
	private Academic academic;
	
	/** A class will take a place in a specific period */
	@ManyToOne
	private Period period;
	
	/** A class is related to a course */
	@ManyToOne
	private Course course;
	
	/** A class is for a specific subject */
	@ManyToOne
	private Subject subject;

	
	@OneToMany(mappedBy="class_", cascade = CascadeType.ALL)
	List<Allocation> allocations;
	
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public int getSlots() {
		return slots;
	}

	public void setSlots(int slots) {
		this.slots = slots;
	}

	public Academic getAcademic() {
		return academic;
	}

	public void setAcademic(Academic academic) {
		this.academic = academic;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	@Override
	public int compareTo(Class o) {
		return uuid.compareTo(o.uuid);
	}
}
