package br.ufes.inf.nemo.marvin.core.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.core.domain.Subject;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;

@Local
public interface ManageSubjectService extends CrudService<Subject> {

	public List<Subject> listSubjects();
}
