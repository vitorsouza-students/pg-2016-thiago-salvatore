package br.ufes.inf.nemo.marvin.core.persistence;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.PersistentObjectNotFoundException;

@Stateless
public class CourseJPADAO extends BaseJPADAO<Course> implements CourseDAO {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;

	/** The logger. */
	private static final Logger logger = Logger.getLogger(AcademicJPADAO.class.getCanonicalName());

	/** The application's persistent context provided by the application server. */
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Class<Course> getDomainClass() {
		return Course.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public List<Course> retriveCourseByNameCode(String name, String code) throws PersistentObjectNotFoundException, MultiplePersistentObjectsFoundException{
		String jpql = "select c from Course c where c.name = :name or c.code = :code";
		TypedQuery<Course> query = entityManager.createQuery(jpql, Course.class);
		query.setParameter("name", name);
		query.setParameter("code", code);
		return query.getResultList();
	}

}
