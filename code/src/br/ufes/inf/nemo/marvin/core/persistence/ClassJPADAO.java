package br.ufes.inf.nemo.marvin.core.persistence;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.marvin.core.domain.Subject;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.PersistentObjectNotFoundException;

@Stateless
public class ClassJPADAO extends BaseJPADAO<br.ufes.inf.nemo.marvin.core.domain.Class> implements ClassDAO {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;

	/** The logger. */
	private static final Logger logger = Logger.getLogger(ClassJPADAO.class.getCanonicalName());

	@PersistenceContext
	private EntityManager entityManager;
	
	
	@Override
	public Class<br.ufes.inf.nemo.marvin.core.domain.Class> getDomainClass() {
		return br.ufes.inf.nemo.marvin.core.domain.Class.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	public List<br.ufes.inf.nemo.marvin.core.domain.Class> retrieveClassByPeriod(Period period) {
		String jpql = "select c from Class c where c.period = :period";
		TypedQuery<br.ufes.inf.nemo.marvin.core.domain.Class> query = entityManager.createQuery(jpql, br.ufes.inf.nemo.marvin.core.domain.Class.class);
		query.setParameter("period", period);
		return query.getResultList();
	}

	@Override
	public List<br.ufes.inf.nemo.marvin.core.domain.Class> retrieveClassByNumberPeriod(String number, Period period) throws PersistentObjectNotFoundException, MultiplePersistentObjectsFoundException{
		String jpql = "select c from Class c where c.number = :number and c.period = :period";
		TypedQuery<br.ufes.inf.nemo.marvin.core.domain.Class> query = entityManager.createQuery(jpql, br.ufes.inf.nemo.marvin.core.domain.Class.class);
		query.setParameter("number", number);
		query.setParameter("period", period);
		return query.getResultList();
	}

	@Override
	public List<br.ufes.inf.nemo.marvin.core.domain.Class> retrieveClassByCourse(Course course) {
		String jpql = "select c from Class c where c.course = :course";
		TypedQuery<br.ufes.inf.nemo.marvin.core.domain.Class> query = entityManager.createQuery(jpql, br.ufes.inf.nemo.marvin.core.domain.Class.class);
		query.setParameter("course", course);
		return query.getResultList();
	}

	@Override
	public List<br.ufes.inf.nemo.marvin.core.domain.Class> retrieveClassBySubject(Subject subject) {
		String jpql = "select c from Class c where c.subject = :subject";
		TypedQuery<br.ufes.inf.nemo.marvin.core.domain.Class> query = entityManager.createQuery(jpql, br.ufes.inf.nemo.marvin.core.domain.Class.class);
		query.setParameter("subject", subject);
		return query.getResultList();
	}
}
