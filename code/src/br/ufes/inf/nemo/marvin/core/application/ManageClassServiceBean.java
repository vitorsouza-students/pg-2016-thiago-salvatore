package br.ufes.inf.nemo.marvin.core.application;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.marvin.core.domain.Class;
import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.marvin.core.domain.Subject;
import br.ufes.inf.nemo.marvin.core.persistence.ClassDAO;
import br.ufes.inf.nemo.util.ejb3.application.CrudException;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.PersistentObjectNotFoundException;

@Stateless
public class ManageClassServiceBean extends CrudServiceBean<Class> implements ManageClassService {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	private List<Class> classes;
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageClassServiceBean.class.getCanonicalName());
	@EJB
	private ClassDAO classDAO;
	
	@PostConstruct
	public void init(){
		classes = this.listClasses();
	}
	
	@Override
	public BaseDAO<Class> getDAO() {
		return classDAO;
	}

	@Override
	protected Class createNewEntity() {
		return new Class();
	}
	
	public List<Class> retrieveClassByPeriod(Period period){
		return classDAO.retrieveClassByPeriod(period);
	}

	@Override
	public List<Class> listClasses() {
		return classDAO.retrieveAll();
	}
	
	@Override
	public void validateCreate(Class entity) throws CrudException {
		// Possibly throwing a CRUD Exception to indicate validation error.
		CrudException crudException = null;
		String crudExceptionMessage = "The class \"" + entity.getNumber() + "\" cannot be created due to validation errors.";
		
		//Business rules
		//Rule 1 - There cannot exist two classes with the same number in the same period
		try {
			List<Class> classes = classDAO.retrieveClassByNumberPeriod(entity.getNumber(), entity.getPeriod());
			if(!classes.isEmpty()){
				logger.log(Level.INFO, "Creation of class \"{0}\" violates validation rule 1: class with id {1} has same number and period", new Object[] { entity.getNumber(), classes.get(0).getId() });
				crudException = addValidationError(crudException, crudExceptionMessage, "manageClass.error.create.same_number_period");
			}
		}
		catch(PersistentObjectNotFoundException e){
			logger.log(Level.FINE, "Rule 1 OK - there`s no other class with the same number and period: {0}", new Object[] {entity.getNumber()});
		}
		catch (MultiplePersistentObjectsFoundException e){
			logger.log(Level.WARNING, "Creation of class \"" + entity.getNumber() + "\" threw an exception: a query for class with this name and period returned multiple results!", e);
			crudException = addValidationError(crudException, crudExceptionMessage, "number_period", "installSystem.button.registerAdministrator");
		}
		if(crudException != null)
			throw crudException;
	}

	@Override
	public void validateUpdate(Class entity) throws CrudException {
		// Possibly throwing a CRUD Exception to indicate validation error.
		CrudException crudException = null;
		String crudExceptionMessage = "The class \"" + entity.getNumber() + "\" cannot be updated due to validation errors.";
		
		//Business rules
		//Rule 1 - There cannot exist two classes with the same number in the same period
		try {
			List<Class> classes = classDAO.retrieveClassByNumberPeriod(entity.getNumber(), entity.getPeriod());
			if(!classes.isEmpty()){
				if(classes.get(0).getId()!=entity.getId()){
					logger.log(Level.INFO, "Update of class \"{0}\" violates validation rule 1: class with id {1} has same number and period", new Object[] { entity.getNumber(), classes.get(0).getId() });
					crudException = addValidationError(crudException, crudExceptionMessage, "manageClass.error.create.same_number_period");	
				}
			}
		}
		catch(PersistentObjectNotFoundException e){
			logger.log(Level.FINE, "Rule 1 OK - there`s no other class with the same number and period: {0}", new Object[] {entity.getNumber()});
		}
		catch (MultiplePersistentObjectsFoundException e){
			logger.log(Level.WARNING, "Update of class \"" + entity.getNumber() + "\" threw an exception: a query for class with this name and period returned multiple results!", e);
			crudException = addValidationError(crudException, crudExceptionMessage, "number_period", "installSystem.button.registerAdministrator");
		}
		if(crudException != null)
			throw crudException;
	}

	@Override
	public List<Class> getClasses() {
		return classes;
	}

	@Override
	public void setClasses(List<Class> classes) {
		this.classes = classes;
	}

	@Override
	public List<Class> retrieveClassByCourse(Course selectedEntity) {
		return classDAO.retrieveClassByCourse(selectedEntity);
	}

	@Override
	public List<Class> retrieveClassBySubject(Subject selectedEntity) {
		return classDAO.retrieveClassBySubject(selectedEntity);
	}

}
