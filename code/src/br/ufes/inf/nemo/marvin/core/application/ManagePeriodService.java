package br.ufes.inf.nemo.marvin.core.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;

@Local
public interface ManagePeriodService extends CrudService<Period> {
	
	public List<Period> getPeriods();
	
	public void setPeriods(List<Period> periods);
}
