package br.ufes.inf.nemo.marvin.core.domain;

import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport_;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-14T20:31:28.227-0300")
@StaticMetamodel(Period.class)
public class Period_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<Period, Integer> year;
	public static volatile SingularAttribute<Period, Integer> number;
	public static volatile SingularAttribute<Period, Date> initialDate;
	public static volatile SingularAttribute<Period, Date> finalDate;
	public static volatile ListAttribute<Period, Class> classes;
}
