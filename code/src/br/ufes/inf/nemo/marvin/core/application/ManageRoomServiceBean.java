package br.ufes.inf.nemo.marvin.core.application;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.marvin.alocaweb.domain.Allocation;
import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.marvin.core.domain.Room;
import br.ufes.inf.nemo.marvin.core.persistence.RoomDAO;
import br.ufes.inf.nemo.util.ejb3.application.CrudException;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.PersistentObjectNotFoundException;

@Stateless
public class ManageRoomServiceBean extends CrudServiceBean<Room> implements ManageRoomService {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageRoomServiceBean.class.getCanonicalName());
	
	@EJB
	private RoomDAO roomDAO;
	
	@Override
	public BaseDAO<Room> getDAO() {
		return roomDAO;
	}

	@Override
	protected Room createNewEntity() {
		return new Room();
	}

	@Override
	public List<Room> listRooms() {
		return roomDAO.retrieveAll();
	}

	@Override
	public void validateCreate(Room entity) throws CrudException {
		// Possibly throwing a CRUD Exception to indicate validation error.
		CrudException crudException = null;
		String crudExceptionMessage = "The room \"" + entity.getNumber() + "\" cannot be created due to validation errors.";
		
		//Business rules
		//Rule 1 - Cannot have two rooms in the same building and floor and number
		try {
			List<Room> rooms = roomDAO.retrieveByNumberBuildingFloor(entity.getNumber(), entity.getBuildingCode(), entity.getBuildingFloor());
			if(!rooms.isEmpty()){
				logger.log(Level.INFO, "Creation of room \"{0}\" violates validation rule 1: room with id {2} has same number floor and building", new Object[] { entity.getNumber(), rooms.get(0).getId() });
				crudException = addValidationError(crudException, crudExceptionMessage, "manageRoom.error.create.same_number_floor_building");
			}
		}
		catch(PersistentObjectNotFoundException e){
			logger.log(Level.FINE, "Rule 1 OK - there`s no other room with the same number, floor and building: {0}", new Object[] {entity.getNumber()});
		}
		catch (MultiplePersistentObjectsFoundException e){
			logger.log(Level.WARNING, "Creation of room \"" + entity.getNumber() + "\" threw an exception: a query for room with this number returned multiple results!", e);
			crudException = addValidationError(crudException, crudExceptionMessage, "floor_number_building", "installSystem.button.registerAdministrator");
		}
		if(crudException != null)
			throw crudException;
	}

	@Override
	public void validateUpdate(Room entity) throws CrudException {
		// Possibly throwing a CRUD Exception to indicate validation error.
		CrudException crudException = null;
		String crudExceptionMessage = "The room \"" + entity.getNumber() + "\" cannot be updated due to validation errors.";
		
		//Business rules
		//Rule 1 - Cannot have two rooms in the same building and floor and number
		//Rule 2 - The number of slots cannot be changed if there is a class with a number of slots higher than the new amount
		try {
			List<Room> rooms = roomDAO.retrieveByNumberBuildingFloor(entity.getNumber(), entity.getBuildingCode(), entity.getBuildingFloor());
			if(!rooms.isEmpty()){
				if(rooms.get(0).getId()!=entity.getId()){
					logger.log(Level.INFO, "Update of room \"{0}\" violates validation rule 1: room with id {2} has same number floor and building", new Object[] { entity.getNumber(), rooms.get(0).getId() });
					crudException = addValidationError(crudException, crudExceptionMessage, "manageRoom.error.create.same_number_floor_building");	
				}
			}
			
			List<Allocation> allocations = roomDAO.retrieveAllocationsByRoom(entity);
			if(!allocations.isEmpty()){
				if(allocations.get(0).getId()!=entity.getId()){
					logger.log(Level.INFO, "Update of room \"{0}\" violates validation rule 2: there is an allocation with the number of slots greater than the new amount", new Object[] { entity.getNumber() });
					crudException = addValidationError(crudException, crudExceptionMessage, "allocation_slots");			
				}
			}
		}
		catch(PersistentObjectNotFoundException e){
			logger.log(Level.FINE, "Rule 1 OK - there`s no other room with the same number, floor and building: {0}", new Object[] {entity.getNumber()});
		}
		catch (MultiplePersistentObjectsFoundException e){
			logger.log(Level.WARNING, "Update of room \"" + entity.getNumber() + "\" threw an exception: a query for room with this number returned multiple results!", e);
			crudException = addValidationError(crudException, crudExceptionMessage, "floor_number_building", "installSystem.button.registerAdministrator");
		}
		if(crudException != null)
			throw crudException;
	}

}
