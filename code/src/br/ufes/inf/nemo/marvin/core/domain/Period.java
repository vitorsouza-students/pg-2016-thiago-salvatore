package br.ufes.inf.nemo.marvin.core.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.ufes.inf.nemo.marvin.people.domain.Person;
import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;

@Entity
public class Period extends PersistentObjectSupport implements Comparable<Period> {
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	
	/** The year that refers to this period */
	@Basic
	@NotNull
	private int year;
	
	/** The semester related to this period */
	@Basic
	@NotNull
	private int number;
	
	/** The date when this period begins and finishes */
	@Temporal(TemporalType.DATE)
	@NotNull
	private Date initialDate;
	
	@Temporal(TemporalType.DATE)
	@NotNull
	private Date finalDate;
	
	@OneToMany(mappedBy="period", cascade = CascadeType.ALL)
	private List<Class> classes;

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	@Override
	public int compareTo(Period o) {
		return uuid.compareTo(o.uuid);
	}
	
}
