package br.ufes.inf.nemo.marvin.core.domain;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;

@Entity
public class Course extends PersistentObjectSupport implements Comparable<Course> {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;

	@Basic
	@NotNull
	private String name;
	
	@Basic
	@NotNull
	private String code;
	
	@OneToMany(mappedBy="course", cascade = CascadeType.ALL)
	private List<Class> classes;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public int compareTo(Course o) {
		return uuid.compareTo(o.uuid);
	}
	
}
