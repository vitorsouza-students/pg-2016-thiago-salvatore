package br.ufes.inf.nemo.marvin.core.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;

@Local
public interface ManageCourseService extends CrudService<Course> {

	public List<Course> listCourses();
}
