package br.ufes.inf.nemo.marvin.core.domain;

import br.ufes.inf.nemo.marvin.alocaweb.domain.Allocation;
import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport_;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-04T15:27:37.299-0300")
@StaticMetamodel(Room.class)
public class Room_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<Room, String> buildingCode;
	public static volatile SingularAttribute<Room, Integer> buildingFloor;
	public static volatile SingularAttribute<Room, Integer> number;
	public static volatile SingularAttribute<Room, Integer> slots;
	public static volatile ListAttribute<Room, Allocation> allocations;
}
