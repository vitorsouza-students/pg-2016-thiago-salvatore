package br.ufes.inf.nemo.marvin.core.application;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.core.persistence.CourseDAO;
import br.ufes.inf.nemo.util.ejb3.application.CrudException;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.PersistentObjectNotFoundException;

@Stateless
public class ManageCourseServiceBean extends CrudServiceBean<Course> implements ManageCourseService {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageCourseServiceBean.class.getCanonicalName());
	
	@EJB
	private CourseDAO courseDAO;
	
	@Override
	public BaseDAO<Course> getDAO() {
		return courseDAO;
	}

	@Override
	protected Course createNewEntity() {
		return new Course();
	}

	@Override
	public List<Course> listCourses() {
		return courseDAO.retrieveAll();
	}
	
	@Override
	public void validateCreate(Course entity) throws CrudException {
		// Possibly throwing a CRUD Exception to indicate validation error.
		CrudException crudException = null;
		String crudExceptionMessage = "The course \"" + entity.getCode() + "\" cannot be created due to validation errors.";
		
		//Business rules
		//Rule 1 - There cannot exist two courses with the same name or code
		try {
			List<Course> courses = courseDAO.retriveCourseByNameCode(entity.getName(), entity.getCode());
			if(!courses.isEmpty()){
				logger.log(Level.INFO, "Creation of course \"{0}\" violates validation rule 1: course with id {1} has same code and name", new Object[] { entity.getCode(), courses.get(0).getId() });
				crudException = addValidationError(crudException, crudExceptionMessage, "manageCourse.error.create.same_code_name");
			}
		}
		catch(PersistentObjectNotFoundException e){
			logger.log(Level.FINE, "Rule 1 OK - there`s no other course with the same name and code: {0}", new Object[] {entity.getCode()});
		}
		catch (MultiplePersistentObjectsFoundException e){
			logger.log(Level.WARNING, "Creation of course \"" + entity.getCode() + "\" threw an exception: a query for course with this name and code returned multiple results!", e);
			crudException = addValidationError(crudException, crudExceptionMessage, "name_code", "installSystem.button.registerAdministrator");
		}
		if(crudException != null)
			throw crudException;
	}

	@Override
	public void validateUpdate(Course entity) throws CrudException {
		// Possibly throwing a CRUD Exception to indicate validation error.
		CrudException crudException = null;
		String crudExceptionMessage = "The course \"" + entity.getCode() + "\" cannot be updated due to validation errors.";
		
		//Business rules
		//Rule 1 - There cannot exist two courses with the same name or code
		try {
			List<Course> courses = courseDAO.retriveCourseByNameCode(entity.getName(), entity.getCode());
			if(!courses.isEmpty()){
				if(courses.get(0).getId()!=entity.getId()){
					logger.log(Level.INFO, "Update of course \"{0}\" violates validation rule 1: course with id {1} has same code and name", new Object[] { entity.getCode(), courses.get(0).getId() });
					crudException = addValidationError(crudException, crudExceptionMessage, "manageCourse.error.create.same_code_name");	
				}
			}
		}
		catch(PersistentObjectNotFoundException e){
			logger.log(Level.FINE, "Rule 1 OK - there`s no other course with the same name and code: {0}", new Object[] {entity.getCode()});
		}
		catch (MultiplePersistentObjectsFoundException e){
			logger.log(Level.WARNING, "Update of course \"" + entity.getCode() + "\" threw an exception: a query for course with this name and code returned multiple results!", e);
			crudException = addValidationError(crudException, crudExceptionMessage, "name_code", "installSystem.button.registerAdministrator");
		}
		if(crudException != null)
			throw crudException;
	}
}
