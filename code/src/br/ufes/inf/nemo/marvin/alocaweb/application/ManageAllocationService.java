package br.ufes.inf.nemo.marvin.alocaweb.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.alocaweb.domain.Allocation;
import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.marvin.core.domain.Room;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;

@Local
public interface ManageAllocationService extends CrudService<Allocation> {
	public List<Allocation> getAllocationsByRoomPeriod(Room room, Period period);
	public List<Allocation> retrieveByPeriod(Period p);
}
