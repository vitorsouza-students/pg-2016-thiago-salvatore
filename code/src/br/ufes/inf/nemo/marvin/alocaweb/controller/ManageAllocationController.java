package br.ufes.inf.nemo.marvin.alocaweb.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import br.ufes.inf.nemo.marvin.alocaweb.application.ManageAllocationService;
import br.ufes.inf.nemo.marvin.alocaweb.domain.Allocation;
import br.ufes.inf.nemo.marvin.core.application.ManagePeriodService;
import br.ufes.inf.nemo.marvin.core.controller.ManageAcademicController;
import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.marvin.core.domain.Room;
import br.ufes.inf.nemo.util.ejb3.application.CrudService;
import br.ufes.inf.nemo.util.ejb3.application.filters.MultipleChoiceFilter;
import br.ufes.inf.nemo.util.ejb3.controller.CrudController;

@Named
@SessionScoped
public class ManageAllocationController extends CrudController<Allocation> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageAcademicController.class.getCanonicalName());
	
	@EJB
	private ManageAllocationService manageAllocationService;
	
	@EJB
	private ManagePeriodService managePeriodService;
	
	private List<Date> timesOfDay;
	
	private List<Date> availableInitialTimes;
	
	private List<Date> availableFinalTimes;
	
	private SimpleDateFormat df;
	
	private Period period;
	
	public ManageAllocationController() {
		 viewPath = "/alocaWeb/manageAllocation/";
	     bundleName = "msgsCore";
	}
	

	@PostConstruct
	public void init(){
		
		df = new SimpleDateFormat("HH:mm");
		
		try {
			timesOfDay = new ArrayList<Date>();
			timesOfDay.add(df.parse("7:00"));
			timesOfDay.add(df.parse("8:00"));
			timesOfDay.add(df.parse("9:00"));
			timesOfDay.add(df.parse("10:00"));
			timesOfDay.add(df.parse("11:00"));
			timesOfDay.add(df.parse("12:00"));
			timesOfDay.add(df.parse("13:00"));
			timesOfDay.add(df.parse("14:00"));
			timesOfDay.add(df.parse("15:00"));
			timesOfDay.add(df.parse("16:00"));
			timesOfDay.add(df.parse("17:00"));
			timesOfDay.add(df.parse("18:00"));
			timesOfDay.add(df.parse("19:00"));
			timesOfDay.add(df.parse("20:00"));
			timesOfDay.add(df.parse("21:00"));
			timesOfDay.add(df.parse("22:00"));	
		}catch(ParseException e){
			logger.log(Level.SEVERE, "Could not parse the times to date");
		}
		
		availableInitialTimes = null;
	}
	
	@Override
	protected CrudService<Allocation> getCrudService() {
		return manageAllocationService;
	}

	@Override
	protected Allocation createNewEntity() {
		return new Allocation();
	}

	

	public List<Date> getAvailableInitialTimes() {
		return availableInitialTimes;
	}


	public void setAvailableInitialTimes(List<Date> availableTimes) {
		this.availableInitialTimes = availableTimes;
	}
	

	public List<Date> getAvailableFinalTimes() {
		return availableFinalTimes;
	}


	public void setAvailableFinalTimes(List<Date> availableFinalTimes) {
		this.availableFinalTimes = availableFinalTimes;
	}

	
	public SimpleDateFormat getDf() {
		return df;
	}


	public DayOfWeek[] getDayOfWeek(){
		return DayOfWeek.values();
	}

	@Override
	protected void initFilters() {
		List<Period> periods = managePeriodService.getPeriods();
		Map<String, String> labels = new LinkedHashMap<String, String>();
		for(Period period : periods)
			labels.put(period.getId().toString(), Integer.toString(period.getYear())+"/"+Integer.toString(period.getNumber()));
		addFilter(new MultipleChoiceFilter<Period>("manageAllocation.filter.byPeriod", "class_.period", "Period", periods, labels));
	
		
	}
	
	/*
	 * This function is used to make sure that two classes cannot happen in the same time and in the same room
	 * 
	 */
	public void updateAvailableTimes(ValueChangeEvent event){
		List<Date> all_times = timesOfDay;
		Room selectedRoom = (Room) event.getNewValue();
		List<Allocation> allocations = manageAllocationService.getAllocationsByRoomPeriod(selectedRoom, selectedEntity.getClass_().getPeriod());
	
		DayOfWeek dayOfWeek;
		
		for(Allocation a : allocations){
			dayOfWeek = a.getDayOfWeek();
			
			all_times = all_times.stream()
					.filter(r -> a.getInitialTime().after(r) || r.after(a.getFinalTime())).collect(Collectors.toList());	
		}
		availableInitialTimes = all_times;
	}
	
	public void updateAvailableFinalTimes(ValueChangeEvent event){
		List<Date> all_times = timesOfDay;
		Date initialTime = (Date) event.getNewValue();
		
		availableFinalTimes = all_times.stream()
				.filter(t -> t.after(initialTime)).collect(Collectors.toList());
		
	}
	
	public void createPDF(){
		
		Period p = this.period;
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		ec.setResponseHeader("Content-type", "application/pdf");
		ec.setResponseHeader("Content-Disposition", "attachment; filename=report_allocation.pdf");

		Document document = new Document(PageSize.A4.rotate());
		
		try {
			PdfWriter writer = PdfWriter.getInstance(document, ec.getResponseOutputStream());
			document.open();
			
			Font font = new Font(FontFamily.HELVETICA, 12, Font.BOLD);
			PdfPTable table = new PdfPTable(7);
			
			List<Allocation> allocations;
			PdfPCell cell;
			allocations = manageAllocationService.retrieveByPeriod(p);
			cell = new PdfPCell(new Phrase("Room allocation for " + String.valueOf(p.getYear())+"/"+String.valueOf(p.getNumber()), font));
			cell.setBackgroundColor(GrayColor.GRAYWHITE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setMinimumHeight(30);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setColspan(7);
			table.addCell(cell);
			table.addCell("Course Name");
			table.addCell("Day of Week");
			table.addCell("Time");
			table.addCell("Building");
			table.addCell("Room #");
			table.addCell("Subject");
			table.addCell("Professor");
			
			for(Allocation a : allocations){
				table.addCell(a.getClass_().getCourse().getName());
				table.addCell(a.getDayOfWeek().name());
				table.addCell(df.format(a.getInitialTime()) + "-" + df.format(a.getFinalTime()));
				table.addCell(a.getRoom().getBuildingCode());
				table.addCell(String.valueOf(a.getRoom().getNumber()));
				table.addCell(a.getClass_().getSubject().getName());
				table.addCell(a.getClass_().getAcademic().getName());
			}
			table.setWidthPercentage(100);
			table.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			document.add(table);
			document.close();
			FacesContext.getCurrentInstance().responseComplete();
			
		} catch (DocumentException e) {
			logger.log(Level.WARNING, e.getMessage());
		} catch (IOException e) {
			logger.log(Level.WARNING,e.getMessage());
		}
	}


	public Period getPeriod() {
		return period;
	}


	public void setPeriod(Period period) {
		this.period = period;
	}
	

}
