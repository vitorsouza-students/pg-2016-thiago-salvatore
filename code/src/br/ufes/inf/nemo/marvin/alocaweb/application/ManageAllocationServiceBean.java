package br.ufes.inf.nemo.marvin.alocaweb.application;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.marvin.alocaweb.domain.Allocation;
import br.ufes.inf.nemo.marvin.alocaweb.persistence.AllocationDAO;
import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.marvin.core.domain.Room;
import br.ufes.inf.nemo.util.ejb3.application.CrudException;
import br.ufes.inf.nemo.util.ejb3.application.CrudServiceBean;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.PersistentObjectNotFoundException;

@Stateless
public class ManageAllocationServiceBean extends CrudServiceBean<Allocation> implements ManageAllocationService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private AllocationDAO allocationDAO;
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageAllocationServiceBean.class.getCanonicalName());
	
	@Override
	public AllocationDAO getDAO() {
		return allocationDAO;
	}

	@Override
	protected Allocation createNewEntity() {
		return new Allocation();
	}
	
	public List<Allocation> getAllocationsByRoomPeriod(Room room, Period period){
		return allocationDAO.getAllocationsByRoomPeriod(room, period);
	}

	@Override
	public void validateCreate(Allocation entity) throws CrudException {
		// Possibly throwing a CRUD Exception to indicate validation error.
		CrudException crudException = null;
		String crudExceptionMessage = "This allocation cannot be created due to validation errors.";
		
		//Business rules
		//Rule 1 - A professor cannot teach two classes at the same time
		//Rule 2 - A class can be allocated in a specific time only if there is no other allocation for this class at this time
		//Rule 3 - A room can be occupied only by one class at a specific time and day of week
		try {
			List<Allocation> allocations = allocationDAO.retrieveAllocationByProfessorTime(entity.getClass_(), entity.getDayOfWeek(), entity.getInitialTime(), entity.getFinalTime());
			if(!allocations.isEmpty()){
				logger.log(Level.INFO, "Creation of allocation \"{0}\" violates validation rule 1: allocation with id {2} has a professor teaching in the same day and time", new Object[] { entity.getId(), allocations.get(0).getId() });
				crudException = addValidationError(crudException, crudExceptionMessage, "manageAllocation.error.create.professor_same_time");
			}
			
			allocations = allocationDAO.retrieveAllocationsByClassRoomTime(entity.getClass_(), entity.getRoom(), entity.getInitialTime(), entity.getFinalTime(), entity.getDayOfWeek());
			
			if(!allocations.isEmpty()){
				logger.log(Level.INFO, "Creation of allocation \"{0}\" violates validation rule 2: allocation with id {2} has class allocated at the same time", new Object[] { entity.getId(), allocations.get(0).getId() });
				crudException = addValidationError(crudException, crudExceptionMessage, "manageAllocation.error.create.class_same_time");				
			}
			
			allocations = allocationDAO.retrieveAllocationsByRoomTime(entity.getRoom(), entity.getInitialTime(), entity.getFinalTime(), entity.getDayOfWeek());
			
			if(!allocations.isEmpty()){
				logger.log(Level.INFO, "Creation of allocation \"{0}\" violates validation rule 3: allocation with id {2} is related to the same room at the same time and day", new Object[] { entity.getId(), allocations.get(0).getId() });
				crudException = addValidationError(crudException, crudExceptionMessage, "manageAllocation.error.create.room_same_time");				
			}
		}
		catch(PersistentObjectNotFoundException e){
			logger.log(Level.FINE, "Rule 1 OK - there`s no other allocation for this professor at the same time: {0}", new Object[] {entity.getId()});
		}
		catch (MultiplePersistentObjectsFoundException e){
			logger.log(Level.WARNING, "Creation of allocation \"" + entity.getId() + "\" threw an exception: a query for allocation with this id returned multiple results!", e);
			crudException = addValidationError(crudException, crudExceptionMessage, crudExceptionMessage, "installSystem.button.registerAdministrator");
		}
		if(crudException != null)
			throw crudException;
	}

	@Override
	public void validateUpdate(Allocation entity) throws CrudException {
		// Possibly throwing a CRUD Exception to indicate validation error.
		CrudException crudException = null;
		String crudExceptionMessage = "The allocation \"" + entity.getId() + "\" cannot be created due to validation errors.";
		
		//Business rules
		//Rule 1 - A professor cannot teach two classes at the same time
		//Rule 2 - A class cannot be allocated to two different rooms at the same time
		try {
			List<Allocation> allocations = allocationDAO.retrieveAllocationByProfessorTime(entity.getClass_(), entity.getDayOfWeek(), entity.getInitialTime(), entity.getFinalTime());
			if(!allocations.isEmpty()){
				if(allocations.get(0).getId()!=entity.getId()){
					logger.log(Level.INFO, "Update of allocation \"{0}\" violates validation rule 1: allocation with id {2} has a professor teaching in the same day and time", new Object[] { entity.getId(), allocations.get(0).getId() });
					crudException = addValidationError(crudException, crudExceptionMessage, "manageAllocation.error.create.professor_same_time");					
				}

			}
			allocations = allocationDAO.retrieveAllocationsByClassRoomTime(entity.getClass_(), entity.getRoom(), entity.getInitialTime(), entity.getFinalTime(), entity.getDayOfWeek());
			if(!allocations.isEmpty()){
				if(allocations.get(0).getId()!=entity.getId()){
					logger.log(Level.INFO, "Update of allocation \"{0}\" violates validation rule 2: allocation with id {2} has class allocated at the same time", new Object[] { entity.getId(), allocations.get(0).getId() });
					crudException = addValidationError(crudException, crudExceptionMessage, "manageAllocation.error.create.class_same_time");				
				}
			}
			allocations = allocationDAO.retrieveAllocationsByRoomTime(entity.getRoom(), entity.getInitialTime(), entity.getFinalTime(), entity.getDayOfWeek());
			
			if(!allocations.isEmpty()){
				if(allocations.get(0).getId()!=entity.getId()){
					logger.log(Level.INFO, "Update of allocation \"{0}\" violates validation rule 3: allocation with id {2} is related to the same room at the same time and day", new Object[] { entity.getId(), allocations.get(0).getId() });
					crudException = addValidationError(crudException, crudExceptionMessage, "manageAllocation.error.create.room_same_time");				
				}
			}
		}
		catch(PersistentObjectNotFoundException e){
			logger.log(Level.FINE, "Rule 1 OK - there`s no other allocation for this professor at the same time: {0}", new Object[] {entity.getId()});
		}
		catch (MultiplePersistentObjectsFoundException e){
			logger.log(Level.WARNING, "Update of allocation \"" + entity.getId() + "\" threw an exception: a query for allocation with this id returned multiple results!", e);
			crudException = addValidationError(crudException, crudExceptionMessage, "professor_allocation", "installSystem.button.registerAdministrator");
		}
		if(crudException != null)
			throw crudException;
	}
	
	public List<Allocation> retrieveByPeriod(Period p){
		return allocationDAO.retrieveAllocationByPeriod(p);
	}
	
}
