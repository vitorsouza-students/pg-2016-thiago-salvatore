package br.ufes.inf.nemo.marvin.alocaweb.persistence;

import java.time.DayOfWeek;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.ufes.inf.nemo.marvin.alocaweb.domain.Allocation;
import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.marvin.core.domain.Room;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseJPADAO;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.PersistentObjectNotFoundException;

@Stateless
public class AllocationJPADAO extends BaseJPADAO<Allocation> implements AllocationDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(AllocationJPADAO.class.getCanonicalName());

	@PersistenceContext
	private EntityManager entityManager;
	

	@Override
	public Class<Allocation> getDomainClass() {
		return Allocation.class;
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	public List<Allocation> getAllocationsByRoomPeriod(Room room, Period period){
		
		String jpql = "SELECT a FROM Allocation a WHERE a.room = :room and a.class_.period = :period";
		TypedQuery<Allocation> query = entityManager.createQuery(jpql,Allocation.class);
		query.setParameter("room", room);
		query.setParameter("period", period);
		return query.getResultList();
	}

	@Override
	public List<Allocation> retrieveAllocationByProfessorTime(br.ufes.inf.nemo.marvin.core.domain.Class class_, DayOfWeek dayOfWeek,
			Date initialTime, Date finalTime) throws PersistentObjectNotFoundException, MultiplePersistentObjectsFoundException {
		String jpql = "select a from Allocation a where a.class_.academic = :academic and a.dayOfWeek = :dayOfWeek and ((a.finalTime > :initialTime and a.finalTime <= :finalTime) or (a.initialTime>= :initialTime and a.initialTime<= :finalTime) or (a.initialTime < :initialTime and a.finalTime > :finalTime)) and a.class_.period = :period";
		TypedQuery<Allocation> query = entityManager.createQuery(jpql, Allocation.class);
		query.setParameter("academic", class_.getAcademic());
		query.setParameter("dayOfWeek", dayOfWeek);
		query.setParameter("initialTime", initialTime);
		query.setParameter("finalTime", finalTime);
		query.setParameter("period", class_.getPeriod());
		
		return query.getResultList();
	}

	@Override
	public List<Allocation> retrieveAllocationsByClassRoomTime(br.ufes.inf.nemo.marvin.core.domain.Class class_,
			Room room, Date initialTime, Date finalTime, DayOfWeek dayOfWeek) {
		String jpql = "select a from Allocation a where a.class_ = :class and a.dayOfWeek = :dayOfWeek and ((a.finalTime > :initialTime and a.finalTime <= :finalTime) or (a.initialTime>= :initialTime and a.initialTime<= :finalTime) or (a.initialTime < :initialTime and a.finalTime > :finalTime))";
		TypedQuery<Allocation> query = entityManager.createQuery(jpql, Allocation.class);
		query.setParameter("class", class_);
		query.setParameter("dayOfWeek", dayOfWeek);
		query.setParameter("initialTime", initialTime);
		query.setParameter("finalTime", finalTime);
		
		return query.getResultList();
	}

	@Override
	public List<Allocation> retrieveAllocationsByRoomTime(Room room, Date initialTime, Date finalTime, DayOfWeek dayOfWeek) {
		String jpql = "select a from Allocation a where a.room = :room and a.dayOfWeek = :dayOfWeek and ((a.finalTime > :initialTime and a.finalTime <= :finalTime) or (a.initialTime>= :initialTime and a.initialTime<= :finalTime) or (a.initialTime < :initialTime and a.finalTime > :finalTime))";
		TypedQuery<Allocation> query = entityManager.createQuery(jpql, Allocation.class);
		query.setParameter("room", room);
		query.setParameter("dayOfWeek", dayOfWeek);
		query.setParameter("initialTime", initialTime);
		query.setParameter("finalTime", finalTime);
		
		return query.getResultList();
	}

	@Override
	public List<Allocation> retrieveAllocationByPeriod(Period p) {
		String jpql = "select a from Allocation a where a.class_.period = :period order by a.room.number, a.initialTime";
		TypedQuery<Allocation> query = entityManager.createQuery(jpql, Allocation.class);
		query.setParameter("period", p);
		
		return query.getResultList();
	}

}
