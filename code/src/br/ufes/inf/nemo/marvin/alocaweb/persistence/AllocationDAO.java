package br.ufes.inf.nemo.marvin.alocaweb.persistence;

import java.time.DayOfWeek;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.alocaweb.domain.Allocation;
import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.core.domain.Class;
import br.ufes.inf.nemo.marvin.core.domain.Period;
import br.ufes.inf.nemo.marvin.core.domain.Room;
import br.ufes.inf.nemo.util.ejb3.persistence.BaseDAO;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.MultiplePersistentObjectsFoundException;
import br.ufes.inf.nemo.util.ejb3.persistence.exceptions.PersistentObjectNotFoundException;

@Local
public interface AllocationDAO extends BaseDAO<Allocation> {
	public List<Allocation> getAllocationsByRoomPeriod(Room room, Period period);

	public List<Allocation> retrieveAllocationByPeriod(Period p);
	
	public List<Allocation> retrieveAllocationByProfessorTime(Class class_, DayOfWeek dayOfWeek,
			Date initialTime, Date finalTime) throws PersistentObjectNotFoundException, MultiplePersistentObjectsFoundException;

	public List<Allocation> retrieveAllocationsByClassRoomTime(Class class_, Room room, Date initialTime,
			Date finalTime, DayOfWeek dayOfWeek);

	public List<Allocation> retrieveAllocationsByRoomTime(Room room, Date initialTime, Date finalTime, DayOfWeek dayOfWeek);
}
