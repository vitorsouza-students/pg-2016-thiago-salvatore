package br.ufes.inf.nemo.marvin.alocaweb.domain;

import java.time.DayOfWeek;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.marvin.core.domain.Class;
import br.ufes.inf.nemo.marvin.core.domain.Room;
import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport;

@Entity
public class Allocation extends PersistentObjectSupport implements Comparable<Allocation> {

	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@NotNull
	private Room room;

	@ManyToOne
	@NotNull
	private Class class_;
	
	@Temporal(TemporalType.TIME)
	@NotNull
	private Date initialTime;
	
	@Temporal(TemporalType.TIME)
	@NotNull
	private Date finalTime;
	
	/* If date is null, this allocation is valid for every week until the end of the period */
	
	@Temporal(TemporalType.DATE)
	private Date date;
	
	@Enumerated(EnumType.STRING)
	private DayOfWeek dayOfWeek;

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Class getClass_() {
		return class_;
	}

	public void setClass_(Class class_) {
		this.class_ = class_;
	}

	public Date getInitialTime() {
		return initialTime;
	}

	public void setInitialTime(Date initialTime) {
		this.initialTime = initialTime;
	}

	public Date getFinalTime() {
		return finalTime;
	}

	public void setFinalTime(Date finalTime) {
		this.finalTime = finalTime;
	}

	public DayOfWeek getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(DayOfWeek dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int compareTo(Allocation o) {
		return uuid.compareTo(o.uuid);
	}
	
}
