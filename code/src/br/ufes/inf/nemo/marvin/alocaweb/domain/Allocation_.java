package br.ufes.inf.nemo.marvin.alocaweb.domain;

import br.ufes.inf.nemo.marvin.core.domain.Class;
import br.ufes.inf.nemo.marvin.core.domain.Room;
import br.ufes.inf.nemo.util.ejb3.persistence.PersistentObjectSupport_;
import java.time.DayOfWeek;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-04T15:27:36.477-0300")
@StaticMetamodel(Allocation.class)
public class Allocation_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<Allocation, Room> room;
	public static volatile SingularAttribute<Allocation, Class> class_;
	public static volatile SingularAttribute<Allocation, Date> initialTime;
	public static volatile SingularAttribute<Allocation, Date> finalTime;
	public static volatile SingularAttribute<Allocation, Date> date;
	public static volatile SingularAttribute<Allocation, DayOfWeek> dayOfWeek;
}
