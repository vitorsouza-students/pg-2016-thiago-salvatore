package br.ufes.inf.nemo.marvin.converters;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.core.application.ManageSubjectService;
import br.ufes.inf.nemo.marvin.core.domain.Subject;

@Named
@SessionScoped
public class SubjectConverter implements Converter, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@EJB
	private ManageSubjectService manageSubjectService;
	
	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
		if(value!=null && value.trim().length()>0){
			try {
				return manageSubjectService.getDAO().retrieveById(Long.valueOf(value));
			} catch(NumberFormatException e) {
				throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid subject"));
			}
		}else{
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		if(object !=null){
			return String.valueOf(((Subject) object).getId());
		}else{
			return null;
		}
	}
}
