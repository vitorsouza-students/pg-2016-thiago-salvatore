% ==============================================================================
% TCC - Thiago Salvatore
% Capítulo 2 - Referencial Teórico
% ==============================================================================
\chapter{Referencial Teórico}
\label{sec-referencial}

Este capítulo apresenta os principais conceitos teóricos que fundamentaram o desenvolvimento dos módulos AlocaWeb e BibLattes e está organizado em 3 seções. A Seção~\ref{sec-referencial-engenharia-software} aborda a Engenharia de Software, destacando os principais conceitos e processos utilizados. A Seção~\ref{sec-referencial-frameweb} apresenta o método FrameWeb. A Seção~\ref{sec-referencial-desenvolvimento-web} apresenta os principais conceitos de desenvolvimento Web.





%%% Início de seção. %%%
\section{Engenharia de Software}
\label{sec-referencial-engenharia-software}

O desenvolvimento de software tem crescido bastante nos últimos anos devido a sua grande importância na sociedade contemporânea. O crescimento do uso de computadores
pessoais, nas organizações e nas diversas áreas do conhecimento humano gerou uma crescente demanda por soluções que realizam a automatização dos processos~\cite{lima-pg15}.

Antes de definir o que é Engenharia de Software, é importante diferenciar o que é programação básica e o que é desenvolvimento de sistemas. Programação básica, como é visto principalmente dentro da universidade, não lida com uma enorme quantidade de linhas de código, com diferentes módulos se comunicando simultaneamente ou com integração com ferramentas externas. Já o desenvolvimento de software lida com tudo isso, e, neste momento, aparece a grande necessidade da \textbf{Engenharia de Software}, que tem como objetivo melhorar a qualidade dos produtos de software e aumentar a produtividade no processo de desenvolvimento. 

A \textbf{Engenharia de Software} trata de aspectos relacionados ao estabelecimento de processos, métodos, técnicas, ferramentas e ambientes de suporte ao desenvolvimento de software~\cite{falboEngSoft}.
Um processo de software pode ser visto como o conjunto de atividades, métodos e práticas que guiam os profissionais na produção de software~\cite{falboEngSoft}. Um processo de software, em uma abordagem de Engenharia de Software, envolve diversas atividades que podem ser classificadas quanto ao seu propósito em~\cite{falboEngReq}:

\begin{itemize}
	\item \textit{Atividades de Desenvolvimento (ou Técnicas)}: são as atividades diretamente relacionadas ao processo de desenvolvimento do software. De maneira geral, este processo envolve as seguintes atividades: Análise e Especificação de Requisitos, Projeto, Implementação, Testes, Entrega e Implantação do Sistema. Veremos um pouco mais sobre cada uma delas a seguir;
	\item \textit{Atividades de Gerência}: envolvem atividades relacionadas ao gerenciamento do projeto de maneira abrangente. Incluem, dentre outras: processo de Gerência de Projetos, processo de Gerência de Configuração, processo de Gerência de Reutilização, etc;
	\item \textit{Atividades de Controle da Qualidade}: são aquelas relacionadas com a avaliação da qualidade do produto em desenvolvimento e do processo de software utilizado. 
\end{itemize}


\subsection{Análise e Especificação de Requisitos}
\label{sec-referencial-engenharia-software-atividade-desenvolvimento-analise-especificacao-requisisto}

A especificação de requisitos compreende a primeira etapa do processo de desenvolvimento de um \textit{software}. Nesta etapa são definidas as funcionalidades que o sistema deve conter e os critérios de qualidade sob os quais o sistema deverá operar. É caracterizada por um esforço conjunto entre clientes, usuários e especialistas de domínio a fim de traçar uma linha conjunta de entendimento entre todas as partes envolvidas a respeito de como um \textit{software} se comportará e de como será utilizado pelo usuário. Trata-se de uma atividade complexa que não se resume somente a perguntar às pessoas o que elas desejam, mas sim analisar cuidadosamente a organização, o domínio da aplicação e os processos de negócio no qual o sistema será utilizado \cite{sommerville1998requirements}.

Uma parte essencial dessa fase é a elaboração de modelos descrevendo o que o software tem de fazer (e não como fazê-lo), dita Modelagem Conceitual. Até este momento, a ênfase está sobre o domínio do problema e não se deve pensar na solução técnica, computacional a ser adotada~\cite{falboEngReq}.

Pode-se dizer que os requisitos de um sistema incluem especificações dos serviços que o sistema deve prover, restrições sob as quais ele deve operar, propriedades gerais do sistema e restrições que devem ser satisfeitas no seu processo de desenvolvimento~\cite{falboEngReq}. Requisitos de \textit{software} podem ser divididos em funcionais e não funcionais.

Requisitos funcionais são declarações de serviços que o sistema deve prover, descrevendo o que o sistema deve fazer~\cite{sommerville2003engenharia}. Já os não funcionais, descrevem restrições sobre os serviços ou funções oferecidas pelo sistema~\cite{sommerville2003engenharia}, as quais limitam as opções para criar uma solução para o problema.

Um importante modelo neste processo é o modelo de casos de uso. O modelo de casos de uso é um modelo comportamental que demonstra as funções do sistema, mas de maneira estática. Ele é composto de dois tipos principais de artefatos: os diagramas de casos de uso e as descrições de casos de uso. Um diagrama de casos de uso é um diagrama bastante simples, que descreve o sistema, seu ambiente e como sistema e ambiente estão relacionados. As descrições dos casos de uso descrevem o passo a passo para a realização dos casos de uso e são essencialmente textuais~\cite{falboEngReq}. 

Os casos de uso cadastrais de baixa complexidade que envolvem as funcionalidades criar, alterar, excluir e consultar, podem ser descritos na forma de tabela como proposto por~\citeonline{falboEngReq}.

Ainda como produto dessa fase do desenvolvimento de um \textit{software}, a modelagem conceitual estrutural visa modelar de forma computacional os requisitos e os casos de uso identificados anteriormente. Por meio deste modelo, é possível identificar de forma clara os tipos de entidade e os tipos de relacionamentos presentes no sistema. O propósito da modelagem conceitual estrutural, segundo o paradigma orientado a objetos, é definir as classes, atributos e associações que são relevantes para tratar o problema a ser resolvido \cite{falboEngReq}.


\subsection{Projeto de Sistemas e Implantação}
\label{sec-referencial-engenharia-software-atividade-desenvolvimento-projeto}

A fase de Projeto durante a especificação de um software tem por objetivo definir e especificar uma solução a ser implementada~\cite{falboProjeto}. Diferentemente da etapa de Análise e Levantamento de Requisitos, onde é definido o que o sistema irá oferecer, quais as necessidades do sistema, etc., esta fase é responsável por definir como serão implementadas as funcionalidades propostas, as tecnologias a serem utilizadas para desenvolvimento e para armazenamento de dados, etc. Esta fase pode ser decomposta em duas etapas menores, a saber: (i) Projeto de Arquitetura do Sistema e (ii) Projeto Detalhado.

A etapa (i) tem como objetivo obter os relacionamentos dos componentes do sistema, assim como identificar tais componentes. Essa arquitetura deve descrever a estrutura de nível mais alto da aplicação e identificar seus principais componentes. O propósito da segunda etapa é detalhar o projeto do software para cada componente identificado na etapa anterior. Os componentes de software devem ser sucessivamente refinados em níveis maiores de detalhamento, até que possam ser codificados e testados~\cite{falboEngSoft}. 

Ao final da fase de Projeto de \textit{Software}, tem-se a arquitetura na qual o sistema será montado juntamente com o projeto dos componentes do sistema. Esses componentes, como proposto em~\cite{fowler2002}, podem ser classificados da seguinte forma: interface com o usuário, lógica de negócio e persistência de dados. O primeiro é responsável por gerenciar toda a interação existente entre sistema e usuário. O segundo tem a função de implementar todas as regras de negócio e requisitos do sistema, aplicando as lógicas de negócio e repassando os dados vindos da camada de interface com o usuário para a camada de persistência. A Camada de Persistência tem como principal função armazenar os dados vindos das outras camadas e, quando necessário, deve permitir a recuperação destes dados.

%%% Início de seção. %%%
\section{Desenvolvimento Web}
\label{sec-referencial-desenvolvimento-web}

Aplicações Web fazem parte do cotidiano de praticamente todas as pessoas com acesso à Internet. Como qualquer outra tecnologia, a Web vem crescendo e se modificando dia após dia, tornando cada vez mais importante o desenvolvimento de aplicações que possam ser facilmente modificadas ou atualizadas. Junto com este crescimento, uma vasta quantidade de \textit{frameworks} acabam surgindo, alguns para solucionar problemas, outros para facilitar a implementação de certas coisas.

Com o objetivo de desenvolver tais aplicações tornou-se essencial a aplicação de conceitos existentes na Engenharia de Software, adaptados para essa nova plataforma, no desenvolvimento das aplicações Web. Características do ambiente Web, como concorrência, carga imprevisível, disponibilidade, sensibilidade ao conteúdo, evolução dinâmica, imediatismo, segurança e estética~\cite{pressmannSoftEng} deram origem a uma nova sub-área da Engenharia de Software, a Engenharia Web.


A Engenharia Web (\textit{Web Engineering - WebE}), é a Engenharia de Software aplicada ao Desenvolvimento Web~\cite{pressmannSoftEng}. Com a existência da Engenharia Web, alguns atributos de qualidade utilizados na Engenharia de Software surgiram como sendo de extrema importância no desenvolvimento de sistemas Web, são eles~\cite{Olsina2001}:

\begin{itemize}
	\item \textbf{Usabilidade}: trata-se de um requisito de qualidade como também um objetivo de aplicações Web: permitir a acessibilidade do sistema. Logo, o site deve ter uma inteligibilidade global, permitir o feedback e ajuda online, planejamento da interface/aspectos estéticos e aspectos especiais (acessibilidade por deficientes);
	\item \textbf{Funcionalidade}: o software Web deve ter capacidade de busca e recuperação de informações/links/funções, aspectos navegacionais e relacionados ao domínio da aplicação;
	\item \textbf{Eficiência}: o tempo de resposta deve ser inferior a 10s (velocidade na geração de páginas/gráficos);
	\item \textbf{Confiabilidade}: relativa à correção no processamento de links, recuperação de erros, validação e recuperação de entradas do usuário;
	\item \textbf{Manutenibilidade}: existe uma rápida evolução tecnológica aliada à necessidade de atualização constante do conteúdo e das informações disponibilizadas na Web, logo o software Web deve ser fácil de corrigir, adaptar e estender.	
\end{itemize}


A Java EE (Java Platform, Enterprise Edition) é uma plataforma padrão para desenvolver aplicações Java de grande porte e/ou para a Internet, que inclui bibliotecas e funcionalidades para implementar software Java distribuído, baseado em componentes modulares que executam em servidores de aplicações e que suportam escalabilidade, segurança, integridade e outros requisitos de aplicações corporativas ou de grande porte~\cite{thiagoJava}.

A plataforma Java EE possui uma série de especificações (tecnologias) com objetivos distintos, por isso é considerada uma plataforma guarda-chuva. As especificações Java EE mais conhecidas são listadas em~\cite{manzoli-pg16}:

\begin{itemize}
	\item \textbf{Servlets}: são componentes Java executados no servidor para gerar conteúdo dinâmico para a Web, como HTML e XML;
	\item \textbf{JSF (JavaServer Faces)}: é um framework Web baseado em Java que tem como objetivo simplificar o desenvolvimento de interfaces de sistemas para a Web, por meio de um modelo de componentes reutilizáveis;
	\item \textbf{JPA (Java Persistence API)}: é uma API padrão do Java para persistência de dados, baseada no conceito de mapeamento objeto-relacional. Essa tecnologia traz alta produtividade para o desenvolvimento de sistemas que necessitam de integração com banco de dados;
	\item \textbf{EJB (Enterprise Java Beans)}: são componentes que executam em servidores de aplicação e possuem como principais objetivos fornecer facilidade e produtividade no desenvolvimento de componentes distribuídos, transacionados, seguros e portáveis;
	\item \textbf{CDI (Contexts and Dependency Injection)}: é a especificação da Java EE que trabalha com injeção de dependências.	
\end{itemize}

Além disso, o Marvin também utiliza uma biblioteca de componentes de interface com o usuário chamada \textit{Primefaces}\footnote{\url{http://www.primefaces.org/}}. Esta biblioteca tem como principal função otimizar o tempo gasto no desenvolvimento de componentes web responsivos, como, por exemplo, tabelas, campos de formulários, botões, etc. Como resultado, a interface se torna mais robusta e agradável para o usuário. 


%%% Início de seção. %%%
\section{O método FrameWeb}
\label{sec-referencial-frameweb}

FrameWeb (\textit{Framework-based Design Method for Web Engineering}) é um método de projeto para construção de sistemas de informação Web (\textit{Web Information Systems – WISs}) baseados em \textit{frameworks}. FrameWeb é baseado em metodologias e linguagens de modelagem bastante difundidas na área de Engenharia de Software sem, no entanto, impor nenhum processo de desenvolvimento específico~\cite{vitorFrameWeb}.


A arquitetura proposta para o método \textit{FrameWeb} foi baseada no padrão Camada de Serviço~\cite{fowler2002}. Tal arquitetura é composta de três camadas~\cite{martins-mestrado15}:

\begin{itemize}
	\item Camada de Negócio (\textit{Business Tier}): responsável pelas funcionalidades relacionadas às regras de negócio da aplicação. Esta Camada é particionada em duas: Lógica de Domínio (\textit{Domain}) e Lógica de Aplicação (\textit{Application});
	\item Camada de Apresentação (\textit{Presentation Tier}): responsável por funcionalidades de interface com o usuário (incluindo as telas que serão apresentadas a ele). Esta Camada, segundo o padrão proposto, é também particionada em duas outras: Visão (\textit{View}) e Controle (\textit{Control});
	\item Camada de Acesso a Dados (\textit{Data Access Tier}): responsável por funcionalidades relacionadas à persistência de dados.
\end{itemize}

O foco do \textit{FrameWeb} é na etapa de Projeto de Sistemas de Informação Web, ou seja, todos os modelos utilizados estão já considerando a arquitetura a ser utilizadas, assim como frameworks, etc.

De acordo com a especificação original do método, a fase de Projeto concentra as propostas principais: (i) definição de uma arquitetura padrão que divide o sistema em camadas, de modo a se integrar bem com os \textit{frameworks} utilizados; (ii) proposta de um conjunto de modelos de projeto que trazem conceitos utilizados pelos \textit{frameworks} para esta fase do processo por meio da criação de um perfil UML que faz com que os diagramas fiquem mais próximos da implementação \cite{vitorFrameWeb}.


A modelagem utilizando o método \textit{FrameWeb} é baseada em quatro principais modelos, que, juntos, são capazes de descrever todo o fluxo de execução de um sistema Web, e além disso, estrutura de banco de dados, de páginas web, etc., são eles:

\begin{itemize}
	\item \textbf{Modelo de Domínio:} é um diagrama de classes da UML que representa o domínio do problema e o mapeamento do mesmo para a estrutura do banco de dados;
	
	\item \textbf{Modelo de Persistência:} é um diagrama utilizado para representar os objetos persistentes (que podem ser salvos no banco de dados) seguindo o padrão de projeto DAO (\textit{Data Access Object})~\cite{alurDAO}.
	
	\item \textbf{Modelo de Navegação:} é utilizado para representar os componentes existentes nos pacotes de Visão (View) e Controles (Controllers), que são responsáveis pela interação com o usuário. Além disso, este modelo é capaz de mostrar o fluxo existente entre diferentes páginas de um sistema web (como certas funcionalidades existentes em uma página, levam o usuário para outra página, e assim sucessivamente).
	
	\item \textbf{Modelo de Aplicação:} é um diagrama utilizado para representar os serviços existentes no pacote de Aplicação (responsáveis por implementar os casos de uso de um sistema) e as dependências de componentes de outras camadas (Controle e Persistência).

\end{itemize}

Esses quatro modelos serão descritos na Seção~\ref{sec-projeto-modelos-frame-web} no contexto dos módulos desenvolvido neste trabalho.