% ==============================================================================
% TCC - Thiago Salvatore
% Capítulo 3 - Projeto Arquitetural e Implementação
% ==============================================================================

\chapter{Projeto Arquitetural e Implementação}
\label{sec-projeto}


Como proposto na Seção~\ref{sec-referencial-engenharia-software}, após as fases de especificação e análise de requisitos, ocorre a fase de projeto do sistema. Esta etapa é responsável pela modelagem de como será a implementação do sistema, incorporando, aos requisitos, as tecnologias a serem utilizadas. 

Neste capítulo iremos mostrar a arquitetura do sistema, assim como algumas partes de sua implementação e apresentar as principais telas do sistema. Na Seção~\ref{sec-projeto-arquitetura-sistema}, a arquitetura do sistema é descrita; na Seção~\ref{sec-projeto-framework-nemo-utils}, o framework nemo-utils é apresentado; na Seção~\ref{sec-projeto-modelos-frame-web} os modelos FrameWeb são apresentados. Por fim, na Seção~\ref{sec-projeto-apresentacao-sistema}, são apresentadas algumas telas e características da ferramenta. 






% ========================================================================================
%   Arquitetura do Sistema
% ========================================================================================

\section{Arquitetura do Sistema}
\label{sec-projeto-arquitetura-sistema}

O sistema Marvin, inicialmente, era composto apenas do subsistema \textit{core} (contendo as principais funcionalidades do mesmo). Com a criação de dois novos módulos (\textit{BibLattes} e \textit{AlocaWeb}), foi necessária, então, a criação de dois novos subsistemas (implementados como pacotes Java), seguindo a divisão de subsistemas feita na análise dos requisitos e apresentada no Capítulo~\ref{sec-requisitos}.

O módulo \texttt{br.ufes.inf.nemo.marvin.core} contém as classes mais importantes do sistema, a saber: Acadêmico, Turmas, Cursos, Períodos, e Disciplinas, e seus respectivos cadastros. Já o módulo \texttt{br.ufes.inf.nemo.marvin.alocaweb} contém as funcionalidades do subsistema \textbf{AlocaWeb}, e o módulo \texttt{br.ufes.inf.nemo.marvin.biblattes} contém as funcionalidades do subsistema \textbf{BibLattes}. Mais à frente iremos detalhar um pouco mais as subdivisões desses módulos.


Estes três módulos são, ainda, subdivididos em camadas segundo a arquitetura proposta na Seção~\ref{sec-referencial-frameweb}. A Figura~\ref{fig-projeto-arquitetura-sistema} mostra tal arquitetura. Os módulos \textit{AlocaWeb} e \textit{BibLattes} foram divido em três camadas, sendo elas: apresentação (\textit{Presentation Tier}), negócio (\textit{Business Tier}) e acesso a dados (\textit{Data Access Tier}). Esta figura mostra, também, as tecnologias Java associadas a cada pacote. Tais tecnologias foram citadas na Seção~\ref{sec-referencial-desenvolvimento-web}. É importante notar a utilização do CDI na ligação entre o pacote \textit{Control} e o pacote \textit{Application} e entre o pacote \textit{Application} e o pacote \textit{Persistence}. A função desta tecnologia é fazer com que um \textit{Managed Bean}, por exemplo, tenha acesso a um \textit{Session Bean} sem ser necessário explicitar isso no código \textit{Java}. Tudo que o programador tem que fazer é declarar uma variável com a anotação específica, e o próprio CDI irá injetar o \textit{Bean} correspondente.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{figuras/projeto/fig-projeto-arquitetura-sistema}
	\caption{Marvin - Arquitetura - Sistema~\cite{lima-pg15}.}	
	\label{fig-projeto-arquitetura-sistema}
\end{figure}

A Figura~\ref{fig-projeto-arquitetura-pacotes} exibe os pacotes que foram criados para os módulos \textit{BibLattes} e \textit{AlocaWeb}. Como podemos perceber, os pacotes foram agrupados por módulos e em seguida de acordo com a arquitetura apresentada na Figura~\ref{fig-projeto-arquitetura-sistema}. A seguir iremos detalhar um pouco mais cada um deles.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.45\textwidth]{figuras/projeto/fig-projeto-arquitetura-pacotes}
	\caption{Marvin - Implementação - Pacotes.}
	\label{fig-projeto-arquitetura-pacotes}
\end{figure}




\subsection{Camada de Apresentação}
\label{sec-projeto-arquitetura-sistema-camada-apresentacao}

A \textbf{camada de apresentação} foi subdividida em visão (\textit{View}) e controle (\textit{Control}). A parte da visão é formada pelas páginas Web. A parte de controle contém os controladores que realizam a comunicação entre a interface e a aplicação.


A estrutura Web do sistema Marvin, cujas páginas Web fazem parte da visão da camada de apresentação está organizada conforme a Figura~\ref{fig-projeto-arquitetura-paginas-web}. Existe uma pasta raiz chamada \texttt{WebContent} que contém todos os arquivos da visão. Esta pasta possui três subpastas que representam os módulos do Marvin, a saber: \texttt{alocaWeb}, \texttt{bibLattes} e \texttt{core}. Dentro da pasta \textit{core}, foi criada uma subpasta para cada um dos casos de uso básicos (CRUD). Isso ajuda na organização e caso seja necessário criar um novo caso de uso, basta adicionar uma nova pasta e os arquivos necessários.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.35\textwidth]{figuras/projeto/fig-projeto-arquitetura-paginas-web}
	\caption{Marvin - Implementação - Páginas Web}
	\label{fig-projeto-arquitetura-paginas-web}
\end{figure}

As pastas que implementam os casos de uso seguem um padrão que foi definido no framework do \textit{nemo-utils} (cf.\ Seção~\ref{sec-projeto-framework-nemo-utils}). Esse padrão de visão consiste em duas páginas, sendo a primeira chamada \texttt{form.xhtml}, que é responsável por elencar os dados das entidades para que possam ser modificados e armazenados no banco de dados. Já a página \texttt{list.xhtml} é responsável por recuperar e exibir para o usuário as informações das entidades que estão armazenadas no banco de dados.


Dentro da pasta raiz \texttt{WebContent}, temos as páginas \texttt{index.html} e \texttt{index.xhtml} que são as páginas iniciais do sistema.


Um decorador é utilizado para definir o layout da página e o menu que está sendo utilizado. A pasta \texttt{resources} contém a subpasta \texttt{default} que contém o decorador. 



\subsection{Camada de Negócio}
\label{sec-projeto-arquitetura-sistema-camada-negócio}

A \textbf{camada de negócio} foi subdividida em domínio (\textit{Domain}) e aplicação (\textit{Application}). A parte do domínio é formada pelas entidades de negócio (classes de domínio), enquanto a aplicação contém as validações dos dados e implementação dos casos de uso (funcionalidades do sistema).

Os pacotes \texttt{core.domain}, \texttt{alocaweb.domain} e \texttt{biblattes.domain} contém a definição de cada uma das classes do sistema Marvin. Cada uma dessas entidades está definida em um arquivo \texttt{*.java} e, através de anotações, identificam como o mapeamento objeto-relacional irá ocorrer. Através desse mapeamento, o JPA irá gerenciar os objetos no banco de dados automaticamente, sem precisar de nenhuma intervenção do desenvolvedor. É nesse momento que é realizado também o relacionamento entre as classes do sistema utilizando as anotações \texttt{@OneToMany}, \texttt{@ManyToOne} ou \texttt{@ManyToMany} de JPA. Por fim, nesse pacote existe um arquivo para cada classe com o mesmo nome e um ``\_'' no final (chamada de \textit{static meta-model} ou metamodelo estático), que declara os atributos que poderão ser utilizados para realizar as consultas no banco de dados utilizando os conceitos de Criteria API. Essas consultas serão implementadas na camada de persistência.

Os pacotes \texttt{core.application}, \texttt{alocaweb.application} e \texttt{biblattes.application} contém os componentes que fazem a comunicação entre a apresentação (controladores) e a persistência (DAOs), implementando as funcionalidades do sistema descritas em seus casos de uso (cf.\ Cap.~\ref{sec-requisitos}). Faz também as validações das informações antes de chamar os métodos de acesso a dados. Essas validações serão feitas ao tentar criar, modificar ou deletar uma entidade.





\subsection{Camada de Acesso a Dados}
\label{sec-projeto-arquitetura-sistema-camada-acesso-dados}

A \textbf{camada de acesso a dados} possui uma única parte responsável pela persistência (\textit{Persistence}) representada pelos pacotes \texttt{core.persistence}, \texttt{alocaweb.persistence} e \texttt{biblattes.persistence}, que contêm os objetos responsáveis por fazer a comunicação com o banco de dados. Esses objetos são conhecidos como DAO (\textit{Data Access Object})~\cite{alurDAO} e serão responsáveis por armazenar e recuperar os dados do banco de dados.

Sobre a arquitetura do banco de dados, conforme explicado anteriormente, o sistema Marvin utiliza o JPA para fazer o mapeamento objeto relacional e, através desse mapeamento, o próprio JPA irá gerenciar os objetos no banco de dados automaticamente. Com isso, foi utilizada a anotação \texttt{@Entity} nas classes do domínio para realizar a persistência dos dados.









% ========================================================================================
%   Framework nemo-utils
% ========================================================================================
\section{Framework \textit{nemo-utils}}
\label{sec-projeto-framework-nemo-utils}

O framework \textit{nemo-utils}\footnote{nemo-utils -- https://github.com/nemo-ufes/nemo-utils}, fornece ao usuário uma série de facilidades no que diz respeito a funcionalidades do tipo CRUD (\textit{create, read, update, delete}). Este framework implementa, genericamente, as operações básicas existentes entre a aplicação e o banco de dados, fazendo com que o desenvolvedor se preocupe apenas em adaptar e indicar quais as entidades de domínio que serão utilizadas e/ou mapeadas, diminuindo, assim, drasticamente o tempo gasto para implementar tais funcionalidades.

O módulo \textit{AlocaWeb} e seus casos de uso são, em boa parte, cadastrais. Por este motivo, praticamente todos os controladores utilizadas pelo módulo AlocaWeb herdam da classe \texttt{CrudController} do \textit{nemo-utils}. Uma outra vantagem deste \textit{framework} é a facilidade na criação de filtros, por exemplo. Através do método \texttt{initFilters}, implementado na classe \texttt{CrudController}, é possível adicionar filtros em uma página de uma maneira extremamente simples.


O \textit{nemo-utils} é composto de algumas classes básicas:

\begin{enumerate}
	\item \textbf{CrudController} - Classe responsável por armazenar dados das páginas \textit{Web} e realizar a comunicação com a camada de aplicação;
	\item \textbf{CrudServiceBean} - Classe responsável por realizar as validações e por fazer a comunicação com a camada de acesso a dados (ao implementar esta classe, o usuário é obrigado a implementar também os métodos de validação existentes nela);
	\item \textbf{PersistentObjectSupport} - Todas as entidades do domínio herdam dessa classe. É uma implementação do padrão para objetos persistentes, que utiliza \texttt{EJB 3} como padrão de anotações para persistência;
	\item \textbf{BaseJPADAO} - Classe responsável por realizar as operações no banco de dados, tais como: consultas, modificação, inserção e exclusão de dados.
\end{enumerate}


Praticamente todas as classes existentes nos módulos \textit{AlocaWeb} e \textit{BibLattes} utilizaram o \textit{nemo-utils} de alguma forma. A principal razão disso foi atender ao requisito não funcional \textbf{RNF - 4} - Reusabilidade. Este requisito diz que o sistema deve explorar o potencial de reutilização. Como dito anteriormente, o objetivo do \textit{nemo-utils} é simplificar a criação de casos de uso cadastrais, evitando que o usuário precise recriar tais funcionalidades para cada entidade de domínio. Por este motivo o uso deste \textit{framework} se tornou essencial.





% ========================================================================================
%   Modelos FrameWeb
% ========================================================================================
\section{Modelos FrameWeb}
\label{sec-projeto-modelos-frame-web}

Nesta seção, são exibidos os modelos FrameWeb, citados anteriormente na Seção~\ref{sec-referencial-frameweb}. Esses modelos também estão divididos nas camadas da arquitetura do sistema, conforme citado na Seção~\ref{sec-projeto-arquitetura-sistema}.

Apresentamos primeiramente o \textbf{Modelo de Domínio}. Os mapeamentos de persistência são meta-dados das classes de domínio que permitem que os \textit{frameworks} ORM (\textit{Object-Relational Mapping}), \textit{frameworks} que realizam o mapeamento objeto-relacional automaticamente, transformem objetos que estão na memória, em linhas de tabelas no banco de dados relacional. Por meio de mecanismos leves de extensão da UML, como estereótipos e restrições, adicionamos tais mapeamentos ao diagrama de classes de domínio, guiando os desenvolvedores na configuração do \textit{framework} ORM. Apesar de tais configurações serem relacionadas mais à persistência do que ao domínio, elas são representadas no Modelo de Domínio porque as classes que são mapeadas e seus atributos são exibidas neste diagrama.

A Figura~\ref{fig-projeto-core-modelo-dominio} mostra o modelo de domínio para o módulo \texttt{Core}. Assim como na implementação do banco de dados, boa parte dos atributos na imagem possuem tamanho (\texttt{size}) definido.  As classes \textbf{Academic} e \textbf{Period} possuem atributos do tipo data. Na restrição destes atributos informamos se precisão vai ser \textit{time}, armazenando no banco de dados somente a hora, \textit{date}, apenas a data, ou \textit{timestamp} armazenado ambos, data e hora. Neste último caso não é preciso colocar na restrição pois é a opção \textit{default}. Além disso, o valor \textit{null} no atributo do tipo Data em \textbf{Academic} significa que o mesmo pode ser nulo no banco de dados.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.9\textwidth]{figuras/projeto/fig-projeto-core-modelo-dominio}
	\caption{FrameWeb - Core - Modelo Domínio.}
	\label{fig-projeto-core-modelo-dominio}
\end{figure}

A Figura~\ref{fig-projeto-alocaweb-modelo-dominio} mostra o modelo de domínio para o módulo \texttt{AlocaWeb}. A classe \textbf{Allocation} está relacionada com as classes \textbf{Room} e \textbf{Class} do módulo \texttt{Core}, que foi definida na Figura~\ref{fig-projeto-core-modelo-dominio}.  

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.9\textwidth]{figuras/projeto/fig-projeto-alocaweb-modelo-dominio.jpg}
	\caption{FrameWeb - AlocaWeb - Modelo Domínio.}
	\label{fig-projeto-alocaweb-modelo-dominio}
\end{figure}

Por último, a Figura~\ref{fig-projeto-biblattes-modelo-dominio} fornece o modelo de domínio para o módulo \texttt{BibLattes}. As classes \textbf{BookChapter}, \textbf{InCollection}, \textbf{Article} e \textbf{InProceeding} são todas tipos específicos de publicação (\textbf{Publication}). Além disso, uma publicação pode possuir diversos autores (\textbf{Author}), mas apenas um dono (\textit{owner}). No contexto deste módulo, dono é a pessoa que fez \textit{upload} do currículo Lattes. Portanto, se dois currículos iguais forem enviados por duas pessoas diferentes, teremos duas publicações iguais no banco, mas com \textit{owners} diferentes.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.9\textwidth]{figuras/projeto/fig-projeto-biblattes-modelo-dominio.jpg}
	\caption{FrameWeb - BibLattes - Modelo Domínio.}
	\label{fig-projeto-biblattes-modelo-dominio}
\end{figure}

Todas as classes de domínio estendem de \textit{PersistentObjectSupport} do framework \textit{nemo-utils}, sendo que essa herança não é mostrada no diagrama com o intuito de não poluí-lo com várias associações.

Diferente da abordagem original do FrameWeb proposto em 2007, todos os atributos que são não nulos tiveram a \textit{tag {not null}} omitida.







Já o \textbf{Modelo de Navegação} é um diagrama de classe da UML que representa os diferentes componentes que formam a camada de Apresentação, como páginas Web, formulários HTML e classes de ação. Esse modelo é utilizado pelos desenvolvedores para guiar a codificação das classes e componentes dos pacotes \textbf{Visão} e \textbf{Controle}.


Em formulários HTML, atributos representam campos do formulário, que devem ter seus tipos definidos de acordo com a biblioteca de componentes utilizada. Como neste trabalho foi utilizado PrimeFaces, os tipos ficarão com o prefixo ``p'' (ex.: \texttt{p.inputText}, \texttt{p.commandButton}, etc.). A classe de ação é o principal componente do modelo. Suas associações de dependência ditam o controle de fluxo quando uma ação é executada.

As funcionalidades criar, editar, excluir e visualizar (abreviadas de CRUD, do inglês \textit{create, read, update e delete}), seguem um mesmo fluxo de execução e de interação com o usuário. Tais funcionalidades são similares para todos os casos de uso cadastrais devido à utilização da ferramenta \textit{nemo-utils}. Esse fluxo de execução similar é representado pela Figura~\ref{fig-projeto-nemo-utils-modelo-navegacao} que é um modelo de apresentação genérico.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/projeto/fig-projeto-nemo-utils-modelo-navegacao}
	\caption{FrameWeb - \textit{nemo-utils} - Modelo Navegação.}
	\label{fig-projeto-nemo-utils-modelo-navegacao}
\end{figure}


A Figura~\ref{fig-projeto-nemo-utils-modelo-navegacao} apresenta o modelo de navegação do framework nemo-utils. Este é um modelo genérico para todas as entidades que são do tipo CRUD (e que utilizam o nemo-utils). Deve existir, para cada entidade, uma página chamada \textit{list.xhtml}, que irá conter a listagem de todos os objetos deste tipo existentes no banco. Essa página possui um formulário relacionado (\textit{manageEntityList}), que armazena todas as entidades sendo mostradas na pagina citada anteriormente. Ao selecionar uma entidade da tabela, a mesma é atribuída ao atributo \textit{entity} existente em \textit{ManageEntityControl}, sendo possível então executar certas ações, tais como: atualizar o objeto selecionado (\textit{update}), ler o objeto selecionado (\textit{retrieve}), e excluir o objeto selecionado (\textit{delete}). Além disso, o usuário pode criar um novo objeto (\textit{create}). Todas estas funcionalidades, ao serem executadas, irão chamar os respectivos métodos existentes em \textit{ManageEntityControl}. Ao executar tais métodos, existem certos retornos, identificados nas relações de dependência saindo de \textit{ManageEntityControl}. Por exemplo, ao executar a funcionalidade \textit{create}, o usuário será redirecionado para a pagina \textit{form.xhtml}, onde poderá incluir os dados do novo objeto, e então executar o método \textit{save}, que irá salvar o objeto, e como retorno, irá ser redirecionado para a página inicial (contendo a listagem de todas as entidades existentes no banco).

Para os casos de uso que apresentam funções diferentes das CRUDs, o modelo anterior não pode ser aplicado. A Figura~\ref{fig-projeto-modelo-navegacao-exportar-oferta} apresenta o modelo de navegação para o fluxo \textit{Exportar Oferta de Disciplinas} do caso de uso \textit{Exportar Oferta de Disciplinas}. É importante notar que a listagem de disciplinas e o filtro por período serão implementados utilizando o \textit{nemo-utils} e, portanto, o passo de filtragem por período não será mostrado no modelo.

Podemos perceber que o modelo possui uma página web marcada com estereótipo \texttt{<<page>>}, Esta página possui um formulário (\textit{manageClassList}), marcado aqui como o estereótipo \texttt{<<form>>} que possui o atributo \textit{period}. Este atributo é utilizado em conjunto com o filtro do \textit{nemo-utils}. Ao selecionar um período, o usuário poderá exportar as ofertas, fazendo com o que o método \texttt{exportClasses()} seja executado. Este método se encontra na classe \texttt{ManageClassController}. O mesmo irá processar a requisição e como resultado, retornará um arquivo no formato PDF (por este motivo o estereótipo \texttt{<<binary>>} no resultado do método \texttt{exportClasses()}).

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/projeto/figura-modelo-view-exportar-oferta}
	\caption{Marvin - AlocaWeb - Exportar Oferta - Modelo Navegação.}
	\label{fig-projeto-modelo-navegacao-exportar-oferta}
\end{figure}



A Figura~\ref{fig-projeto-modelo-navegacao-efetuar-alocacao} exibe o modelo de navegação para o fluxo \textit{Efetuar Alocação} do caso de uso \textit{Gerenciar Alocações} e o fluxo \textit{Exportar Alocação} do caso de uso \textit{Exportar Alocação}. É importante notar neste modelo que, quando o \textit{form} tem um atributo \textbf{X} (neste caso \textit{class}, por exemplo) e o controlador tem um método que recebe tal parâmetro (neste caso o controlador \textit{ManageRoomController} tem o método \textit{updateRooms} que tem como parâmetro \textit{class}), a passagem da informação deve ser feita via chamada do método. Este fluxo não é proposto pelo \textit{FrameWeb}, sendo, portanto, uma sugestão de extensão para o mesmo.

Outro importante detalhe é que o método \textit{createPDF}, como pode ser visto na imagem, realiza a exportação de alocações baseadas em um período (caso selecionado), mas não recebe nenhum parâmetro. Isso acontece porque o \textit{nemo-utils} possui sua própria maneira de implementar os filtros (como pode ser visto na herança existente na Figura~\ref{fig-projeto-modelo-navegacao-efetuar-alocacao}). Dentro de cada controlador que herda de \textit{CrudController}, existe um atributo \textit{filterParam}. Este atributo é usado dentro da função \texttt{createPDF} com o objetivo de retornar (caso selecionado) o período que será utilizado na exportação. O fluxo \textit{Consultar Alocação} do caso de uso \textit{Gerenciar Alocação} corresponde exatamente a esta filtragem por período, portanto também não é exibido no modelo.

\newpage


\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/projeto/figura-modelo-view-gerenciar-alocacoes}
	\caption{Marvin - AlocaWeb - Efetuar Alocação e Exportar Alocação - Modelo Navegação.}
	\label{fig-projeto-modelo-navegacao-efetuar-alocacao}
\end{figure}

O modelo apresentado na Figura~\ref{fig-projeto-modelo-navegacao-efetuar-alocacao} é um pouco mais complexo, pois faz uso de \textit{AJAX} para popular os valores dos campos do formulário. Podemos ver que \texttt{manageAlloca\-tionForm} possui o estereótipo \texttt{<<form>>}, e que praticamente todos os métodos disparados a partir dele são do tipo \textit{AJAX}. A ideia do fluxo acima é que o sistema diminua as opções de seleção do usuário de acordo com o que ele seleciona, ou seja: o usuário seleciona uma turma, o sistema então dispara o método \textit{updateRooms} do controlador \textit{ManageRoomController}. Este método irá buscar todas as salas de aula com capacidade maior ou igual à quantidade de vagas ofertadas na turma (\textit{class}) selecionada, e irá renderizar o campo \textit{room} existente em \textit{manageAllocationForm}.

Um outro fluxo importante neste modelo é o fluxo de exportar alocação. O form \textit{manageAllocationList} é responsável por tal. O usuário irá selecionar um período (como foi explicado anteriormente, o atributo \texttt{filterParam} do nemo-utils é responsável por isso), e irá exportar a alocação. O método createPDF irá, então, gerar um arquivo \texttt{allocations.pdf} contendo todas as alocações para o período selecionado.

Por fim, a Figura~\ref{fig-projeto-modelo-navegacao-enviar-curriculo} mostra o modelo de navegação para o fluxo \textit{Enviar Currículo} do caso de uso \textit{Enviar Currículo Lattes} e o fluxo \textit{Exportar Currículo} do caso de uso \textit{Exportar Currículo}.

\newpage


\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{figuras/projeto/figura-modelo-view-expotar-curriculo}
	\caption{Marvin - BibLattes - Enviar Currículo e Exportar Currículo - Modelo Navegação.}
	\label{fig-projeto-modelo-navegacao-enviar-curriculo}
\end{figure}

No modelo acima temos o form \texttt{formEnviarCurrículo} com atributo file do tipo \textit{inputFile}. Isso significa que o campo do formulário é um campo para \textit{upload} de arquivo. Ao efetuar o \textit{upload}, o form dispara o método \textit{readXML} do controlador \texttt{ManageLattesController}. Este método irá ler o arquivo XML contendo o currículo Lattes e popular a lista de publicações (\textit{publications}). Ao final, a página \texttt{curriculo.xhtml} será renderizada, contendo a listagem de todas as publicações que foram enviadas.

Além disso, o usuário poderá também exportar a lista de publicações no formato BibTex. Ao realizar tal exportação, a página \texttt{curriculo.xhtml} chama o método \textit{exportBibLattes}. Este método irá gerar o arquivo \texttt{curriculo.bib} com todas as publicações.

O próximo modelo FrameWeb a ser mostrado é o \textbf{Modelo de Aplicação}, que consiste em um diagrama de classes da UML que representa as classes de serviço, responsáveis pela codificação dos casos de uso, e suas dependências. Esse diagrama é utilizado para guiar a implementação das classes do pacote \textbf{Aplicação} e a configuração das dependências entre os pacotes \textbf{Controle, Aplicação e Persistência}, ou seja, quais classes de ação dependem de quais classes de serviço e quais DAOs são necessários para que as classes de serviço alcancem seus objetivos~\cite{vitorFrameWeb}.


Todas as classes de aplicação que são de casos de uso cadastrais estendem de \textit{CrudServiceBean} do pacote \textit{nemo-utils}. A Figura~\ref{fig-projeto-nemo-utils-modelo-aplicacao} apresenta o modelo de aplicação do nemo-utils.


\begin{figure}[h]
	\centering
	\includegraphics[width=0.35\textwidth]{figuras/projeto/fig-projeto-nemo-utils-modelo-aplicacao}
	\caption{FrameWeb - \textit{nemo-utils} - Modelo Aplicação~\cite{lima-pg15}.}
	\label{fig-projeto-nemo-utils-modelo-aplicacao}
\end{figure}

\newpage

A Figura~\ref{fig-projeto-core-modelo-aplicacao} mostra o modelo de aplicação para o módulo \texttt{core}. A Figura~\ref{fig-projeto-alocaweb-modelo-aplicacao} mostra o modelo de aplicação para o módulo \texttt{AlocaWeb} e, por último, a Figura~\ref{fig-projeto-biblattes-modelo-aplicacao} mostra o modelo de aplicação para o módulo \texttt{BibLattes}.


\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{figuras/projeto/figura-modelo-aplicacao-core.jpg}
	\caption{FrameWeb - Marvin - Core - Modelo Aplicação.}
	\label{fig-projeto-core-modelo-aplicacao}
\end{figure}


\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{figuras/projeto/figura-modelo-aplicacao-alocaweb.jpg}
	\caption{FrameWeb - Marvin - AlocaWeb - Modelo Aplicação.}
	\label{fig-projeto-alocaweb-modelo-aplicacao}
\end{figure}


\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{figuras/projeto/figura-modelo-aplicacao-biblattes}
	\caption{FrameWeb - Marvin - BibLattes - Modelo Aplicação.}
	\label{fig-projeto-biblattes-modelo-aplicacao}
\end{figure}


\clearpage
\newpage
Outro importante padrão apresentado e sugerido pelo FrameWeb é o DAO. Tal padrão de desenvolvimento é baseado na existência de uma camada de acesso a dados, fazendo com que o controlador não tenha acesso direto ao banco de dados. O \textbf{Modelo de Persistência} é um diagrama de classes da UML que representa as classes DAO existentes, responsáveis pela persistência das instâncias das classes de domínio. Esse diagrama é utilizado para auxiliar no processo de desenvolvimento do pacote de persistência (onde os DAOs ficam localizados).

Existem diversas funcionalidades que são comuns a qualquer classe (ou objeto) que possa ser salvo em um banco de dados, são elas: retornar todos os objetos, deletar um objeto, salvar um objeto, atualizar um objeto, retornar um objeto dado um id, etc. Com o objetivo de evitar a repetição destas funcionalidades em cada DAO, o \textit{nemo-utils} nos provê um DAO base que implementa as principais operações utilizadas em um banco de dados. Além disso, a utilização de interfaces na criação dos DAOs é importante para evitar que qualquer modificação tecnológica (API de persistência, por exemplo), influencie drasticamente em uma aplicação. A Figura~\ref{fig-projeto-nemo-utils-modelo-persistencia} exibe as classes bases do \textit{nemo-utils}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{figuras/projeto/fig-projeto-nemo-utils-modelo-persistencia}
	\caption{FrameWeb - \textit{nemo-utils} - Modelo Persistência~\cite{lima-pg15}.}
	\label{fig-projeto-nemo-utils-modelo-persistencia}
\end{figure}

Com o objetivo de permitir que qualquer classe herde as funcionalidades existentes em seus DAOs base, o \textit{nemo-utils} declara tanto a interface \textbf{BaseDAO} como a classe \textbf{BaseJPADAO} usando tipos genéricos, fazendo com que as classes e interfaces que herdem deles tenham que especificar qual a classe que será gerenciada pelos mesmos. Também não será necessário exibir os métodos do DAO na implementação e na interface, basta modelá-los em apenas um dos dois. No caso do DAO Base, subentende-se que todos os métodos públicos de BaseJPADAO são definidos na interface BaseDAO.

Segundo os padrões estabelecidos por FrameWeb, todas as interfaces DAO são subinterfaces de BaseDAO, enquanto todas as implementações JPA são subclasses de BaseJPADAO, herdando todos os métodos básicos, por exemplo: \texttt{retrieveAll(), save(), delete(), retrieveById()}. Os demais métodos que foram declarados no diagrama se referem a consultas específicas que devem ser disponibilizadas para o funcionamento de determinados casos de uso. 

As Figuras~\ref{fig-projeto-core-modelo-persistencia},~\ref{fig-projeto-alocaweb-modelo-persistencia} e~\ref{fig-projeto-biblattes-modelo-persistencia} são os modelos de persistência para os módulos \texttt{Core}, \texttt{AlocaWeb} e \texttt{BibLattes}, respectivamente. É importante notar aqui que existem alguns modelos que possuem operações específicas (que foram implementadas neste trabalho), como por exemplo \texttt{CourseJPADAO} que possui a operação \textit{retrieveCourseByNameCode}. Este método busca um curso dado o nome e o código do mesmo. Ele teve de ser implementado pois o \textit{framework} nemo-utils não possui essa operação, visto ser ela necessária para atender um determinado caso de uso específico do domínio de nossa aplicação.


\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{figuras/projeto/figura-modelo-persistencia-core}
	\caption{FrameWeb - Marvin - Core - Modelo Persistência.}
	\label{fig-projeto-core-modelo-persistencia}
\end{figure}


\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{figuras/projeto/figura-modelo-persistencia-alocaweb}
	\caption{FrameWeb - Marvin - AlocaWeb - Modelo Persistência.}
	\label{fig-projeto-alocaweb-modelo-persistencia}
\end{figure}


\clearpage
\newpage

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{figuras/projeto/figura-modelo-persistencia-biblattes}
	\caption{FrameWeb - Marvin - AlocaWeb - Modelo Persistência.}
	\label{fig-projeto-biblattes-modelo-persistencia}
\end{figure}


Como é possível perceber, o Modelo de Persistência não define nenhuma extensão da UML para representar os conceitos necessários da camada de acesso a dados, mas apenas regras que tornam essa modelagem mais simples e rápida, por meio da definição de padrões.















% ========================================================================================
%  Apresentando o Sistema
% ========================================================================================
\section{Apresentação do Sistema}
\label{sec-projeto-apresentacao-sistema}

Nesta seção, apresentamos o sistema por meio de uma série de capturas de tela, a Figura~\ref{fig-projeto-apresentacao-login} mostra a tela inicial de login no sistema. As telas de \textit{Login} e \textit{Academics} não foram implementadas neste trabalho, já estavam prontas quando iniciamos a implementação dos módulos AlocaWeb e BibLattes. Elas serão mostradas apenas para que o fluxo não fique sem sentido.


\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/telas/marvin_login}
	\caption{Marvin - Tela Login.}
	\label{fig-projeto-apresentacao-login}
\end{figure}

O login utilizará email e senha. O campo do e-mail possui validação para verificar se o mesmo é válido. O campo da senha aceita qualquer caractere alfanumérico e possui tamanho máximo 15.

Do lado esquerdo da tela é possível ver um menu. Inicialmente este menu possui apenas a funcionalidade Login. Depois de efetuado o mesmo, o usuário poderá ver todas as funcionalidades implementadas e que já existiam no sistema Marvin, como pode ser visto na Figura~\ref{fig-projeto-apresentacao-home}.


\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/telas/manage_submenu}
	\caption{Marvin - Páginal Inicial.}
	\label{fig-projeto-apresentacao-home}
\end{figure}

O sistema possui um layout responsivo, por este motivo, a visualização do menu irá ser diferente em um celular e em um \textit{Desktop}, por exemplo. A Figura~\ref{fig-projeto-apresentacao-menu-mobile} exibe como fica o menu em um celular. Ao contrário da aplicação Desktop, que o menu está sempre visível, no celular o usuário deve clicar no botão com três traços para ativar o mesmo. Nos próximos passos serão mostradas outras telas em um dispositivo móvel.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.3\textwidth]{figuras/projeto/telas/menu_mobile}
	\caption{Marvin - Menu Dispositivo Móvel.}
	\label{fig-projeto-apresentacao-menu-mobile}
\end{figure}


As funcionalidades do tipo CRUD possuem telas que seguem exatamente o mesmo padrão e fluxo. Para evitar repetição, apenas uma das telas será mostrada, exibindo as funcionalidades de listagem, cadastro, leitura, atualização e exclusão de uma turma. A Figura~\ref{fig-projeto-apresentacao-class-list} exibe a tela que lista os cursos cadastrados no sistema.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/telas/list_classes}
	\caption{Marvin - Lista de Turmas.}
	\label{fig-projeto-apresentacao-class-list}
\end{figure}


\clearpage
\newpage

Ao selecionar um item na listagem, por exemplo, a turma de \textit{Software Engineering}, uma lista de funcionalidades aparece, além da funcionalidade \textit{\textbf{New}} que já estava presente, a saber: \textit{View}, \textit{Modify} e \textit{Delete}. Ao clicar nos botões \textbf{New} (novo), \textbf{View} (visualizar) ou \textbf{Modify} (alterar), o usuário será redirecionado para a página de cadastro de um item conforme a Figura~\ref{fig-projeto-apresentacao-class-form}. É importante notar que ao clicar em \textit{View}, os campos do formulário serão somente leitura. Ao clicar em \textit{Modify} os campos do formulário já virão preenchidos com as informações da turma selecionada, sendo possível editar as mesmas. E por último, ao clicar em \textit{New}, o formulário será exibido em branco, possibilitando a criação de uma nova turma.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/telas/new_class}
	\caption{Marvin - Tela Cadastro de Turma.}
	\label{fig-projeto-apresentacao-class-form}
\end{figure}


A funcionalidade \textit{Delete} possibilita a exclusão de um item (neste caso, de uma turma). Ao clicar na mesma, um novo painel será exibido, para confirmação da exclusão. O usuário poderá, então, confirmar a mesma, excluindo o item, ou cancelar. A Figura~\ref{fig-projeto-apresentacao-period-excluir} exibe tal comportamento para o CRUD \textit{Semester}.


\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/telas/period_delete}
	\caption{Marvin - Tela exclusão de Período.}
	\label{fig-projeto-apresentacao-period-excluir}
\end{figure}



A Figura~\ref{fig-projeto-apresentacao-resposiva-buttons} apresenta a tela de listagem de períodos para quem está utilizando um dispositivo móvel. Ao contrário do Desktop, o menu, como dito anteriormente, não fica visível, e os botões de ação estão agora em posição vertical (não mais horizontal).

\begin{figure}[h]
	\centering
	\includegraphics[width=0.3\textwidth]{figuras/projeto/telas/period_selected_mobile}
	\caption{Marvin - Tela Listagem Períodos Mobile.}
	\label{fig-projeto-apresentacao-resposiva-buttons}
\end{figure}


\newpage

A Figura~\ref{fig-projeto-alocaweb-apresentacao-alocacoes} exibe a tela relacionada ao caso de uso \texttt{Gerenciar Alocações}. Esta tela é bastante semelhante às outras, com a diferença de que é possível filtrar os resultados por período. As funcionalidades \textit{New}, \textit{View}, \textit{Modify} e \textit{Delete} não serão exibidas novamente.

 
\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/telas/manage_allocation}
	\caption{AlocaWeb - Gerenciar Alocações.}
	\label{fig-projeto-alocaweb-apresentacao-alocacoes}
\end{figure}

A Figura~\ref{fig-projeto-apresentacao-export-alocacao} mostra a tela responsável por implementar o caso de uso \texttt{Exportar Alocação}. Nesta tela, o usuário irá selecionar um período e clicar em exportar. O sistema irá, então, gerar um arquivo PDF contendo todas as alocações para o período selecionado. A Figura~\ref{fig-projeto-apresentacao-pdf-alocacao} exibe um modelo deste documento PDF.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/telas/export_allocation}
	\caption{AlocaWeb - Exportar Alocações.}
	\label{fig-projeto-apresentacao-export-alocacao}
\end{figure}


\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/telas/room_allocation_pdf}
	\caption{AlocaWeb - Relatório de Alocações.}
	\label{fig-projeto-apresentacao-pdf-alocacao}
\end{figure}


\newpage
Outra importante funcionalidade do sistema está relacionada ao BibLattes. A Figura~\ref{fig-projeto-apresentacao-import-cv} mostra a tela para importação de um currículo lattes. Nesta tela é possível ver o botão \textit{Choose} e \textit{Import}. O primeiro permite ao usuário selecionar o currículo que será enviado, e o segundo executa a ação de importar o currículo. Ao final do processamento, o usuário é automaticamente redirecionado para a tela de gerenciamento de publicações (que pode também ser acessada através do menu BibLattes > Manage Publications).

A Figura~\ref{fig-projeto-apresentacao-manage-publications} exibe a tela para visualização de publicações. Esta tela possui uma das principais funcionalidades do módulo \textit{BibLattes} que é a exportação de publicações para o formato \textit{BibTeX}. Para executar tal ação, o usuário precisa apenas clicar em \textit{Export to BibTeX}.


\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/telas/biblattes_import}
	\caption{BibLattes - Tela Importação Currículo Lattes.}
	\label{fig-projeto-apresentacao-import-cv}
\end{figure}


\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/projeto/telas/biblattes_list}
	\caption{BibLattes - Tela Gerenciamento de Publicações.}
	\label{fig-projeto-apresentacao-manage-publications}
\end{figure}

A Figura~\ref{fig-projeto-apresentacao-gerenciamento-pub-mobile} exibe a tela de gerenciamento de publicações em um dispositivo mobile. É possível perceber que apenas a coluna \textit{Title} é exibida. Como o dispositivo possui uma tela menor, é melhor mostrar ao usuário apenas a informação mais importante, neste caso, o título da publicação.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.3\textwidth]{figuras/projeto/telas/biblattes_list_mobile}
	\caption{BibLattes - Tela Gerenciamento de Publicações Mobile.}
	\label{fig-projeto-apresentacao-gerenciamento-pub-mobile}
\end{figure}